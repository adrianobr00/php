<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/



$route['account/produto-lista'] = 'admin/Produto';
$route['account/produto-add'] = 'admin/produto/produto_add';
$route['account/signle-produto-edita'] = 'admin/produtos/editar_produto';
$route['account/produto_edita_submit'] = 'admin/produtos/produto_editar_submit';
$route['account/produto-edita/(:num)'] = 'admin/produtos/get_ProdutoEditar/$1';
$route['account/produto/produto-code'] = 'admin/Produtos/produto_codigo';
$route['account/produto-status/(:num)/(:num)'] = 'admin/produtos/produto_Status/$1/$2';

$route['account/subcategoria_ver'] = 'admin/produtos/ver_SubCategoria';
$route['account/produto-delete/(:num)'] = 'admin/produto/deletar_Produto/$1';
$route['account/produto-detalhe'] = 'admin/produto/produto_DetalheUnico';

/*----------- VENDAS --------------------------------------------- */
$route['account/venda-lista'] = 'admin/venda';
$route['account/venda-detalhe/(:num)'] = 'admin/venda/detalhe_Venda/$1';
$route['account/venda-status/(:num)'] = 'admin/venda/VendaStatus/$1';
$route['account/venda-lista-enviado'] = 'admin/venda/enviado';

/*----------- CATEGORIA --------------------------------------------- */
$route['account/categoria-add'] = 'admin/categoria/categoria_add';
$route['account/categoria'] = 'admin/categoria';
$route['account/edita-categoria/(:num)'] = 'admin/categoria/editar_Categoria/$1';
$route['account/categoria-delete/(:num)'] = 'admin/categoria/deletar_Categoria/$1';
$route['account/categoria-status/(:num)/(:num)'] = 'admin/categoria/categoria_Status/$1/$2';

/*----------- SUBCATEGORIA --------------------------------------------- */
$route['account/add-subcategoria/(:num)'] = 'admin/categoria/add_SubCategoria/$1';
$route['account/ver-subcategoria/(:num)']	= 'admin/categoria/ver_SubCategoria/$1';
$route['account/edita-subcategoria/(:num)']	= 'admin/categoria/editar_SubCategoria/$1';
$route['account/sub-categoria-delete/(:num)']	= 'admin/categoria/deletar_SubCategoria/$1';

/*----------- SLIDERS --------------------------------------------- */
$route['account/add-slider'] = 'admin/slider/add_Slider';
$route['account/lista-slider'] = 'admin/slider';
$route['account/slider-delete/(:num)'] = 'admin/slider/DeleteSlider/$1';

/*----------- CLIENTES --------------------------------------------- */
$route['account/cliente-lista'] = 'admin/cliente';
$route['account/cliente-delete/(:num)'] = 'admin/cliente/deletar_Cliente/$1';
/*----------- CARRINHO --------------------------------------------- */


$route['mobile-shop'] = 'dashboard/mobile-shop';
$route['cart'] = 'dashboard/cart';
$route['produto-detail/(:num)'] = 'dashboard/produto-detalhe/$1';
$route['add'] = 'dashboard/add';
$route['update-cart'] = 'dashboard/update_cart';
$route['clear-cart'] = 'cart/remove/all';
$route['signle-cart'] = 'dashboard/single_product_add_to_cart';

/*----------- Admin --------------------------------------------- */
$route['account/login'] = 'admin/login_controllers';
$route['account'] = 'admin/dashboard';
$route['account/dashboard'] = 'admin/dashboard';
$route['account/novo-admin'] = 'admin/newadmin/add';
$route['account/admin-lista'] = 'admin/newadmin';
$route['account/admin-delete/(:num)'] = 'admin/newadmin/deletar_Admin/$1';

/*----------- usuario produto --------------------------------------------- */
$route['ProdutoLista/(:num)/(:num)']	= 'produto/allProdutoLista/$1/$2';
$route['ProdutoLista/(:num)/(:num)/(:num)']	= 'produto/allProdutoLista/$1/$2';
$route['ProductDetalhe/(:num)']	= 'product/oneProdutodetalhe/$1';
/*----------- Desejos --------------------------------------------- */
$route['delete-desejo/(:num)']	= 'desejo/DeletelistaDesejo/$1';
$route['categoria/(:num)']	= 'categoriamenu/index/$1';

/*----------- Remessa --------------------------------------------- */
$route['Remessa']	= 'dashboard/Remessa';
$route['term']	= 'dashboard/term';
$route['faqs']	= 'dashboard/faqs';

/*----------- COR --------------------------------------------- */
$route['account/cor-add'] = 'admin/cor/cor_Add';
$route['account/cor'] = 'admin/cor';
$route['account/edita-cor/(:num)'] = 'admin/cor/editar_Cor/$1';
$route['account/cor-delete/(:num)'] = 'admin/cor/deletar_Cor/$1';

/*----------- TAMANHO --------------------------------------------- */
$route['account/tamanho-add'] = 'admin/tamanho/add_tamanho';
$route['account/tamanho'] = 'admin/tamanho';
$route['account/tamanho-edita/(:num)'] = 'admin/tamanho/editar_Tamanho/$1';
$route['account/tamanho-delete/(:num)'] = 'admin/tamanho/deletar_Tamanho/$1';


$route['default_controller'] = "Dashboard";
$route['404_override'] = '';



/* End of file routes.php */
/* Location: ./application/config/routes.php */