
      <!-- Site Footer-->
      <footer class="site-footer" style="background-color: #66ffcc; color: #000;">
        <div class="container" style="color: #000;">
          <div class="row" style="color: #000;">
            <div class="col-lg-6 col-md-6" style="color: #000;">
              <!-- Contact Info-->
              <section class="widget widget-light-skin" style="color: #000;">
                <h3 class="widget-title" style="color: #000;">Entre em contato conosco.</h3>
                <h3 class="widget-title" style="color: #000;">Fone: 61 3391-5304 / 9862-4345</h3>
                <ul class="list-unstyled text-sm text-white" style="color: #000;">
                  <li style="color: #000;"><span class="opacity-50" style="color: #000;">Segunda-Sexta:</span>8.00 - 18.00</li>
                  <li style="color: #000;"><span class="" style="color: #000;">SDS - Bloco A - GALERIA BOULEVARD - CONIC - </span>Brasília - DF</li>
                </ul>
                <p><a class="navi-link-light opacity-50" href="#" style="color: #000;">oticakrystal@gmail.com</a></p>
              </section>
            </div>
            
            <div class="col-lg-6 col-md-6" style="color: #000;">
              <!-- MAP-->
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3839.155416520289!2d-47.886792734630184!3d-15.795757027197777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1136b6955911cd47!2sConic+Bras%C3%ADlia!5e0!3m2!1spt-BR!2sbr!4v1519754717710" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            
          </div>
          <hr class="hr-light mt-2 margin-bottom-2x" style="color: #000;">
          <div class="row">
            <div class="col-md-7 padding-bottom-1x" style="color: #000;">
              <!-- Payment Methods-->
              <div class="margin-bottom-1x" style="max-width: 415px;"><img src="img/BANDEIRAS_CARTAO_01.png" alt="Payment Methods" style="color: #000;">
              </div>
            </div>
            <div class="col-md-5 padding-bottom-1x"style="color: #000;" >
              <div class="margin-top-1x hidden-md-up" style="color: #000;"></div>
              <!--Subscription-->
              <!-- <form class="subscribe-form" action="#" method="post" target="_blank" style="color: #000;">
                <div class="clearfix" style="color: #000;">
                  <div class="input-group" style="color: #000;">
                    <input class="form-control" type="email" name="EMAIL" placeholder="Seu E-mail" style="color: #000;"><span class="input-group-addon"><i class="icon-mail"style="color: #000;" ></i></span>
                  </div>
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups
                  <div style="position: absolute; left: -5000px;" aria-hidden="true" style="color: #000;">
                    <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                  </div>
                  <button class="btn btn-primary" type="submit"style="background-color: #ccc;"><i class="icon-check"></i></button>
                </div><span class="form-text text-sm opacity-50" style="color: #000;">Inscreva-se na nossa Newsletter para receber ofertas de desconto, últimas notícias, vendas e informações promocionais.</span>
              </form> -->
            </div>
          </div>
          <!-- Copyright-->
          <p class="footer-copyright opacity-50" style="color: #000;">© 2018 Todos os Direitos Reservados. <i class="icon-layout text-danger"></i>  Desenvolvido por<i class="icon-code text-danger"></i><a href="http://carloscrdweb.com.br/" target="_blank" style="color: #ff6600;"> &nbsp;carloscrdweb.</a></p>
        </div>
      </footer>
    </div>
    <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="icon-arrow-up"></i></a>
    <!-- Backdrop-->
    <div class="site-backdrop"></div>
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="<?php echo base_url();?>assets/js/vendor.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/scripts.min.js"></script>
  </body>
</html>