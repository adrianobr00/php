

 <header class="navbar navbar-sticky">
      <!-- Search-->
      <form class="site-search" method="get">
        <input type="text" name="site_search" placeholder="Type to search...">
        <div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>
      </form>
      <div class="site-branding">
        <div class="inner">
          <!-- Off-Canvas Toggle (#shop-categories)--><a class="offcanvas-toggle cats-toggle" href="#shop-categories" data-toggle="offcanvas"></a>
          <!-- Off-Canvas Toggle (#mobile-menu)--><a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>
          <!-- Site Logo--><a class="site-logo" href="index.php"><img src="<?=base_url();?>assets/img/logokrystal.png" alt="logoKrystal"></a>
        </div>
      </div>
      <!-- Main Navigation-->
       
      <nav class="site-menu" style="background-color: #66ffcc;">
        <ul>
            <li class="has-megamenu active"> <a  href="<?=base_url();?>"dashboard><span>Home</a></span></a>
            <ul class="mega-menu">
              <li><a class="d-block img-thumbnail text-center navi-link" href="index.php"><img alt="Featured Products Slider" src="img/mega-menu-home/01.png">
                  <h6 class="mt-3">Slide de Produtos em Destaque</h6></a></li>
              <li><a class="d-block img-thumbnail text-center navi-link" href="categorias.php"><img alt="Featured Categories" src="img/mega-menu-home/02.png">
                  <h6 class="mt-3">Categorias em Destaque</h6></a></li>
              
				
            </ul>
            <ul>
                               
				</ul>
          </li>
          
          <li class="has-megamenu"><a href="#"><span>Produtos</span></a>
           <ul class="mega-menu">
             <?php  foreach ($categoria_lista as $category){   ?>   
              <li>  
               <a href="<?php echo base_url()?>ProductList/<?php echo $category->idCategoria;?>"><span class="mega-menu-title"><?= $category->categoria_nome;?></span></a>
              </li>            
                        <?php } ?>
              </ul>
          </li>         
          <li><a href="#"><span>Páginas</span></a>
            <ul class="sub-menu">
                     <?php
                                   
                     if(!empty($this->session->userdata('userid'))) {?>                                
                      <li><a href="<?=base_url();?>login/logout" >Logout</a></li>
                        <?php }
                        else { ?>
                            <li><a href="<?=base_url();?>login">Login</a></li>
                                <li><a href="<?=base_url();?>register">Registrar</a></li>
                       <?php } ?>
            </ul> 
            </li>
              <li><a href="<?=base_url()?>cart"><span>Meu Carrinho</span></a>
            <ul class="sub-menu">
               
                        <?php if(!empty($this->cart->contents())) {
                            echo $this->cart->total_items();
                        } else{
                          echo "0";
                        }  ?>   
                 
                  </li> 
                    </ul>
            </li>                              
            </ul>
            </nav>
      <!-- Toolbar-->
      <div class="toolbar">
        <div class="inner">
          <!-- PHP-->
        </div>
      </div>
    </header>

				
			
<div class="header-bottom">
<div class="container">
<!--/.content-->
<div class="content white">

	    <!--/.navbar-header-->
	
	    <div class="sub-menu" ">
	       
                    <li><a href="<?=base_url();?>dashboard">Home</a></li>
                         <?php
                        foreach ($categoria_lista as $category)
                            
                        {
                        ?>
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$category->categoria_nome?><b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-3">
			            <div class="row">
                                           <?php 
                                                foreach ($subcate as $sub){
                                                    if($category->idCategoria==$sub->parent_categoria_id)
                                                    {
                                                ?>
				            <div class="col-sm-4">
					            <ul class="multi-column-dropdown">
                                                        <li><a style="text-transform: capitalize;" class="list1" href="<?php echo base_url()?>ProductList/<?php echo $category->idCategoria.'/'.$sub->subcat_id;?>"><?= $sub->subcategoria_nome;?></a></li>
					            </ul>
				            </div>
                                                <?php } }?>
				    </div>
		            </ul>
		        </li>
                        <?php }?>
                        <li><a href="<?=base_url();?>contact">Contact Us</a></li>
               
	    </div>
	    <!--/.navbar-collapse-->
	</nav>
	<!--/.navbar-->
</div>







