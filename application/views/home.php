
  <!-- Body-->
  <body>
    <!-- Off-Canvas Category Menu-->
    <div class="offcanvas-container" id="shop-categories">
      <div class="offcanvas-header">
        <h3 class="offcanvas-title">Categorias da Loja</h3>
      </div>
      <nav class="offcanvas-menu">
        <ul class="menu">
             <?php foreach ($categoria_lista as $category){ ?>
                                     
        <ul class="menu">
          <li class="has-children"><span><a href="shop-categorias-oculosGrau.php"><?=$category->categoria_nome?></a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
            </ul>
          </li>
             <?php }?>
           <li class="has-children"><span><a href="sobre.php">Sobre</a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
           </ul>
          </li>
          <li class="has-children"><span><a href="contatos.php">Contato</a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
            </ul>
          </li>
          <li class="has-children"><span><a href="contas.php">Login/Registrar</a><span class="sub-menu-toggle"></span></span>
          <ul class="offcanvas-submenu">
          </ul>
          </li>
        </ul>
      </nav>
    </div>
    <!-- Off-Canvas Mobile Menu-->
    <div class="offcanvas-container" id="mobile-menu">
      <nav class="offcanvas-menu">
           <?php foreach ($categoria_lista as $category){ ?>                               
        <ul class="menu">
          <li class="has-children"><span><a href="shop-categorias-oculosGrau.php"><?=$category->categoria_nome?></a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
            </ul>
          </li>
             <?php }?>
           <li class="has-children"><span><a href="sobre.php">Sobre</a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
           </ul>
          </li>
          <li class="has-children"><span><a href="contatos.php">Contato</a><span class="sub-menu-toggle"></span></span>
            <ul class="offcanvas-submenu">
            </ul>
          </li>
          <li class="has-children"><span><a href="contas.php">Login/Registrar</a><span class="sub-menu-toggle"></span></span>
          <ul class="offcanvas-submenu">
          </ul>
          </li>
        </ul>
      </nav>
    </div>
    <!-- Topbar-->
    <div class="topbar">
      <div class="topbar-column"><a class="hidden-md-down" href="mailto:#"><i class="icon-mail"></i>&nbsp; oticakrystal@gmail.com</a><!-- <a class="social-button sb-facebook shape-none sb-dark" href="#" target="_blank"><i class="socicon-facebook"></i></a><a class="social-button sb-linkedin shape-none sb-dark" href="#" target="_blank"><i class="socicon-linkedin"></i></a> -->
      </div>
      
    </div>
    <!-- Navbar-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
    <?php $this->load->view('tema/menu');?>
   </div>
    <!-- Off-Canvas Wrapper-->
    <div class="offcanvas-wrapper">
      <!-- Page Content-->
      <!-- Main Slider-->
	  </br>
	  <div align="center">
              <img src="<?php base_url()?>assets/img/logoKrystal01_600x200_01.png" alt="logoKrystal" class="img-responsive">
		</div>
	  </br>
          <section class="hero-slider" style="background-image: url(assets/img/main-bg01.jpg);">
        <div class="owl-carousel large-controls dots-inside" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 7000 }">
          <?php foreach ($produtos_destaque as $populares){?>
            <div class="item">
            <div class="container padding-top-3x">
              <div class="row justify-content-center align-items-center">
                <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center">
                    <div class="from-bottom"><img class="d-inline-block w-150 mb-4" src="<?php base_url(); ?>upload/produto/<?php echo $populares->idProdutos.'/'.$populares->product_image; ?>" alt="Logo">
                    <div class="h2 text-body text-normal mb-2 pt-1"><?php echo $populares->descricao; ?></div>
                    <div class="h2 text-body text-normal mb-4 pb-1">A partir de R$:<span class="text-bold"><?php echo $populares->precoVenda; ?></span></div>
                  </div><!-- <a class="btn btn-primary scale-up delay-1" href="shop-categorias-oculosSol.php">Ver Todos</a> -->
                </div>
                <div class="col-md-6 padding-bottom-2x mb-3"><img class="d-block mx-auto" src="<?php base_url(); ?>upload/produto/<?php echo $populares->idProdutos.'/'.$populares->product_image; ?>" alt="Imagem"></div>
              </div>
            </div>
          </div>
	<?php }?>
        </div>
      </section>
      <!-- Top Categories-->
      <section class="container padding-top-3x">
		
        <h3 class="text-center mb-30">Categorias</h3>
         <div class="row">
         
        <?php foreach ($subcate as $subcategoria) {?>
       
          <div class="col-md-6 col-sm-6">
            <div class="card mb-30"><a class="card-img-tiles" href="#">
                <div class="inner">
                  <div class="main-img"><img src="<?= base_url(); ?>upload/subcategoria/<?php echo $subcategoria->subcat_id.'/'.$subcategoria->imagem; ?>" alt="Category"></div>
                  <div class="thumblist"><img src="<?= base_url(); ?>upload/subcategoria/<?php echo $subcategoria->subcat_id.'/'.$subcategoria->imagem; ?>"<img src="<?= base_url(); ?>upload/produto/<?php echo $subcategoria->subcat_id.'/'.$subcategoria->imagem; ?>"</div>
                </div></a>
              <div class="card-body text-center">
                <h4 class="card-title"><?php echo $subcategoria->subcategoria_nome?></h4>
                <p class="text-muted">A partir de R$50.00</p><a class="btn btn-outline-primary btn-sm" href="shop-categorias-oculosSol.php">Vejas os Produtos</a>
              </div>
            </div>
          </div></div>
       
       
         <?php }?> 
       </div>   <div class="row"> 
        <!-- <div class="text-center"><a class="btn btn-outline-secondary margin-top-none" href="#">Todas as Categorias</a></div> -->
      </section>
      <!-- Promo #1-->
      <section class="container-fluid padding-top-3x">
        <div class="row justify-content-center">
          <div class="col-xl-5 col-lg-6 mb-30">
            <div class="rounded bg-faded position-relative padding-top-3x padding-bottom-3x"><span class="product-badge text-danger" style="top: 24px; left: 24px;">Oferta Limitada</span>
              <div class="text-center">
                <h3 class="h2 text-normal mb-1">Lentes de Contato Colorida</h3>
                <h2 class="display-2 text-bold mb-2">a partir de R$59.90</h2>
                <h4 class="h3 text-normal mb-4">Preços Reduzidos!</h4>
                <div class="countdown mb-3" data-date-time="07/31/2018 12:00:00">
                  <div class="item">
                    <div class="days">00</div><span class="days_ref">Dias</span>
                  </div>
                  <div class="item">
                    <div class="hours">00</div><span class="hours_ref">Horas</span>
                  </div>
                  <div class="item">
                    <div class="minutes">00</div><span class="minutes_ref">Min</span>
                  </div>
                  <div class="item">
                    <div class="seconds">00</div><span class="seconds_ref">Sec</span>
                  </div>
                </div><br><a class="btn btn-primary margin-bottom-none" href="#">Veja</a>
              </div>
            </div>
          </div>
          <div class="col-xl-5 col-lg-6 mb-30" style="min-height: 270px;">
            <div class="img-cover rounded" style="background-image: url(img/banners/img01.png);"></div>
          </div>
        </div>
      </section>
      <!-- Promo #2-->
      <section class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-12">
            <div class="fw-section rounded padding-top-4x padding-bottom-4x" style="background-image: url(img/banners/img02.png);"><span class="overlay rounded" style="opacity: .35;"></span>
              <div class="text-center">
                <h3 class="display-4 text-normal text-white text-shadow mb-1">Promoção</h3>
                <h2 class="display-2 text-bold text-white text-shadow">Exame de Vista</h2>
				<h2 class="display-2 text-bold text-white text-shadow">R$50,00</h2>
				<h3 class="display-4 text-normal text-white text-shadow mb-1">Dinheiro ou Cartão</h3>
                <h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">Em nossa Loja de Distribuição</h4><br><a class="btn btn-primary margin-bottom-none" href="contatos.php">Localizar Loja</a>
              </div>
            </div>
          </div>
        </div>
      </section><br>
	  <!-- Promo #3-->
      <section class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-12">
            <div class="fw-section rounded padding-top-4x padding-bottom-4x" style="background-image: url(img/banners/img03.png);"><span class="overlay rounded" style="opacity: .35;"></span>
              <div class="text-center">
                <h3 class="display-4 text-normal text-white text-shadow mb-1">Promoção</h3>
                <h2 class="display-2 text-bold text-white text-shadow">Óculos Completo Armação + Lente</h2>
				<h2 class="display-2 text-bold text-white text-shadow">R$120,00 a vista.</h2>
				
                <h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">Em nossa Loja de Distribuição</h4><br><a class="btn btn-primary margin-bottom-none" href="contatos.php">Localizar Loja</a>
              </div>
            </div>
          </div>
        </div>
      </section><br>
	  <!-- Promo #4-->
      <section class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-12">
            <div class="fw-section rounded padding-top-4x padding-bottom-4x" style="background-image: url(img/banners/img04.png);"><span class="overlay rounded" style="opacity: .35;"></span>
              <div class="text-center">
                <h3 class="display-4 text-normal text-white text-shadow mb-1">Promoção</h3>
                <h2 class="display-2 text-bold text-white text-shadow">Lentes de Grau (miopia)</h2>
				<h2 class="display-2 text-bold text-white text-shadow">1 caixa R$150,00 a vista.</h2>
				<h2 class="display-2 text-bold text-white text-shadow">2 caixa R$190,00 a vista.</h2>
				<h3 class="display-4 text-normal text-white text-shadow mb-1">Bausch+Lomb Softlens 59</h3>
                <h4 class="d-inline-block h2 text-normal text-white text-shadow border-default border-left-0 border-right-0 mb-4">Em nossa Loja de Distribuição</h4><br><a class="btn btn-primary margin-bottom-none" href="contatos.php">Localizar Loja</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Featured Products Carousel-->
      <section class="container padding-top-3x padding-bottom-3x">
        <h3 class="text-center mb-30">Produtos em Destaque</h3>
        <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
          <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
              <div class="product-badge text-danger"></div><a class="product-thumb" href="shopSingle-LentesColor01.php"><img src="img/shop/products/img004.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor01.php">Lentes Coloridas LUNARE descartável</a></h3>
              <h4 class="product-price">
                <!-- <del>R$244.00</del>R$190.00 -->
				R$89,00
              </h4>
              
            </div>
          </div>
          <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor02.php"><img src="img/shop/products/img005.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor02.php">Lentes Coloridas Fresh Look descartável</a></h3>
              <h4 class="product-price">R$60.00</h4>
              
            </div>
          </div>          
          <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor03.php"><img src="img/shop/products/img006.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor03.php">Lentes Coloridas Solótica descartável</a></h3>
              <h4 class="product-price">R$110.00</h4>
              
            </div>
          </div> 
		  <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor04.php"><img src="img/shop/products/img007.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor04.php">Lentes Coloridas Solótica anual</a></h3>
              <h4 class="product-price">R$280.00</h4>
              
            </div>
          </div>
		  <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor05.php"><img src="img/shop/products/img008.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor05.php">Lentes Coloridas Optycolor Magic Top anual</a></h3>
              <h4 class="product-price">R$320.00</h4>
              
            </div>
          </div>
		   <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor06.php"><img src="img/shop/products/img001.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor06.php">Lentes Coloridas Bausch+Lomb anual</a></h3>
              <h4 class="product-price">R$290.00</h4>
              
            </div>
          </div>         
		<!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor07.php"><img src="img/shop/products/img009.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor05.php">Lentes Incolor Acuvue OASIS</a></h3>
              <h4 class="product-price">R$150.00</h4>
              
            </div>
          </div>
		  <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor08.php"><img src="img/shop/products/img010.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor05.php">Lentes Incolor Acuvue - 2</a></h3>
              <h4 class="product-price">R$120.00</h4>
              
            </div>
          </div>
		  <!-- Product-->
          <div class="grid-item">
            <div class="product-card">
                <div class="rating-stars"></div><a class="product-thumb" href="shopSingle-LentesColor09.php"><img src="img/shop/products/img011.png" alt="Lentes de Contato"></a>
              <h3 class="product-title"><a href="shopSingle-LentesColor05.php">Lentes Incolor O2 Optix Alcon </a></h3>
              <h4 class="product-price">R$160.00</h4>
              
            </div>
          </div>
		</div>  
      </section>
      
      <!-- Popular Brands-->
      <section class="bg-faded padding-top-3x padding-bottom-3x">
        <div class="container">
          <h3 class="text-center mb-30 pb-2">Marcas Populares</h3>
          <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2}, &quot;470&quot;:{&quot;items&quot;:3},&quot;630&quot;:{&quot;items&quot;:4}} }"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/01.png" alt="Oriente"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/02.png" alt="Ray-Ban"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/04.png" alt="Keeper"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/06.png" alt="Looping"></div>
        </div>
      </section>
      <!-- Services-->
      <section class="container padding-top-3x padding-bottom-2x">
        <div class="row">
          <!-- <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/01.png" alt="Shipping">
            <h6>ENVIO RÁPIDO E SEGURO</h6>
            <p class="text-muted margin-bottom-none">Entregamos com segurança em qualquer lugar do DF e entorno. Seu produto chega rápido até você!</p>
          </div>
          <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/02.png" alt="Money Back">
            <h6>GARANTIA DE DEVOLUÇÃO DE DINHEIRO</h6>
            <p class="text-muted margin-bottom-none">Nós devolvemos seu dinheiro dentro de 30 dias</p>
          </div> -->
          <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/03.png" alt="Support">
            <h6>8H AS 18H (SEG A SEX)</h6>
            <p class="text-muted margin-bottom-none">Fale diretamente com um de nossos vendedores especializados.</p>
          </div>
          <div class="col-md-3 col-sm-6 text-center mb-30"><img class="d-block w-90 img-thumbnail rounded-circle mx-auto mb-3" src="img/services/04.png" alt="Payment">
            <h6>FORMAS DE PAGAMENTO</h6>
            <p class="text-muted margin-bottom-none">Pagamentos facilitados por meio da utilização de cartão de débito e crédito.</p>
          </div>
        </div>
      </section>
