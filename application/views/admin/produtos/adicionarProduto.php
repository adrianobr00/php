<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}

</style>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################><div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Cadastro de Produto</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal"  enctype="multipart/form-data">
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"><a style="cursor: pointer;" class="AutoGenerate"><i class="icon-plus-sign-alt"></i> Gerar Codigo</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <input class="form-control" type="text" id="produto_code" value="<?php echo set_value('produto_code'); ?>" name="produto_code" placeholder="Clique em Gerar Codigo ao lado" >
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_code'); ?>
                            </div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Nome do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <input class="form-control" type="text" id="produto_nome" value="<?php echo set_value('produto_nome'); ?>" name="produto_nome" placeholder="Digite o nome do Produto" >
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_nome'); ?>
                            </div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                             <textarea class="form-control input-lg" id="descricao"  name="descricao" placeholder="exe: Oculos lente antireflexo, armação de dobravel..." rows="4" ><?php echo set_value('descricao'); ?></textarea>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="descricao" class="control-label">Categoria: <span class="required">*</span></label>
                         <div class="controls">    
                            <select class="form-control" name="produto_categoria" id="ver_SubCategoria" >
                                <option value="">Selecione uma Categoria</option>
                                <?php
                                foreach ($categoria_lista as $post) {?>
                                    <option value="<?=$post->idCategoria?>"><?=$post->categoria_nome?></option>
                               <?php } ?>
                            </select>
                         </div>
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_categoria'); ?></div>
                            <br>
                            
                            <div id="subcat">
                                 <select class="form-control" multiple="" name="subcat_id" >
                                  <?php
                                foreach ($sub_categoria as $post)
                                {?> 
                               <option value="<?=$post->id?>"></option>
                               <?php }
                           ?>
                            </select>
                             <div class="controls">    
                            </div> 
                            </div>
                            <br>  
                    </div>
<!a###################################################################################################################################################################################################################################>
                            <div class="form-group">
                                            <label  class="col-md-2 control-label">Produtos Relacionados<span class="text-danger"></span></label>
                                            <div class="col-md-7">
                                                    <select  class="form-control" name="relacionados[]"  multiple id="relacionados">
                                                   <?php
                                                        foreach ($produto_lista as $post)
                                                        {?>
                                                        <option value="<?=$post->idProdutos?>"><?=$post->produto_nome?></option>
                                                       <?php }
                                                   ?>
                                                   </select>
                                            </div>
                                        </div
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Cor do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                           <div class="controls">
                            <select  class="form-control" name="cor_produto"  multiple id="cor_produto"> <span style="color:red">
                            <?php
                                foreach ($cor_lista as $post){ ?> 
                                <option style="font-size: larger; color:#<?= $post->cor_code?>; " value="<?=$post->id?>"><?= $post->cor_nome;?></option>
                            <?php } ?>
                           </select>
                           </div>   
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Tamanho</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <select   class="form-control" name="tamanho"  multiple id="tamanho">
                                <?php
                                    foreach ($tamanho_lista as $post){?>
                                    <option value="<?=$post->id?>"><?=$post->tamanho;?></option>
                                    <?php } ?>
                            </select>
                        </div>   
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Destaque do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <select class="form-control" name="produto_tipo" >
                                <option value="">Selecione o Tipo</option>
                                <option value="1">Recentes</option>
                                <option value="2">Popular</option>
                                <option value="3">Destaque</option>
                            </select>
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_tipo'); ?></div>
                        </div>   
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label">Tipo de Movimento</label>
                        <div class="controls">
                            <label for="entrada" class="btn btn-default" style="margin-top: 5px;">Entrada 
                                <input type="checkbox" id="entrada" name="entrada" class="badgebox" value="1" checked>
                                <span class="badge" >&check;</span>
                            </label>
                            <label for="saida" class="btn btn-default" style="margin-top: 5px;">Saída 
                                <input type="checkbox" id="saida" name="saida" class="badgebox" value="1" checked>
                                <span class="badge" >&check;</span>
                            </label>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="precoCompra" class="control-label">Preço de Compra<span class="required">*</span></label>
                        <div class="controls">
                            <input id="precoCompra" class="money" type="text" name="precoCompra" value="<?php echo set_value('precoCompra'); ?>"  />
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="precoVenda" class="control-label">Preço de Venda<span class="required">*</span></label>
                        <div class="controls">
                            <input id="precoVenda" class="money" type="text" name="precoVenda" value="<?php echo set_value('precoVenda'); ?>"  />
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="unidade" class="control-label">Unidade<span class="required">*</span></label>
                        <div class="controls">
                            <!--<input id="unidade" type="text" name="unidade" value="<?php echo set_value('unidade'); ?>"  />-->
                            <select id="unidade" name="unidade">
                                <option value="UN">Unidade</option>
                                <option value="KG">Kilograma</option>
                                <option value="LT">Litro</option>
                                <option value="CX">Caixa</option>
                            </select>
                        </div>
                    </div> 
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="estoque" class="control-label">Estoque<span class="required">*</span></label>
                        <div class="controls">
                            <input id="estoque" type="text" name="estoque" value="<?php echo set_value('estoque'); ?>"  />
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="estoqueMinimo" class="control-label">Estoque Mínimo</label>
                        <div class="controls">
                            <input id="estoqueMinimo" type="text" name="estoqueMinimo" value="<?php echo set_value('estoqueMinimo'); ?>"  />
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="estoqueMinimo" class="control-label">Capa</label>
                        <div class="controls">
                            <img id="previewimage" onclick="$('#uploadFile').click();" src="<?php echo base_url(); ?>images/product_image.gif" style="cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;"/>
                            <input type="file" id="uploadFile" name="product_image" style="position: absolute; margin: 0px auto; visibility: hidden;" accept="image/*" />
                            <div style="margin-top: 0px; color: red;"><?= form_error('product_image'); ?></div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                <a href="<?php echo base_url() ?>admin/produtos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>

                </form>
            </div>
         </div>
     </div>
</div>

<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/js/maskmoney.js"></script>

<script>
    
$('document').ready(function()
{ 
	function readURL(input) {
		if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                            $('#previewimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
		}
	}
		$("#uploadFile").change(function(){
			readURL(this);
		});
                
		$(".AutoGenerate").click(function(){
                    
		});
                
        $('.AutoGenerate').bind("click", function() {
            $.post("<?php echo base_url() . 'account/produto/produto-code' ?>", {}, function(data) {
                    $('#produto_code').val(data);
            });
	});
        
        $('#ver_SubCategoria').change(function(event)
        {   
            var categoria_id = $(this).val()
            $.ajax({
			url:'<?=site_url('account/subcategoria_ver')?>',
			data:"categoria_id="+categoria_id,
			type:"post",
			dataType: "json",
			success: function(data) {
				$('#subcat').html(data.html);
			}
		});
        });
        
        
        
        
var quantity_blur=function()
{
    var quantidade = $('#quantidade').val().trim();
    if(quantidade==='' || isNaN(quantidade))
    {
        $('#quantidade').val('1');
        quantidade='1';
    }

    var valor = parseFloat($('#valor').val());
    var bruto = parseFloat(valor * quantidade);
    
    $('#bruto').val(bruto.toFixed(2));
};

var discount_blur=function()
{
    var discount = $('#produto_desconto').val().trim();
    if(discount==='' || isNaN(discount))
    {
        $('#discount').val('0');
        discount='0';
    }
    var bruto = $('#bruto').val();
    var net = bruto - (bruto*discount/100);
    
    $('#net').val(net.toFixed(2));
}

 $('.quantity').blur(quantity_blur);
 $('.discount').blur(discount_blur);
 
 
 
        
       
	
});
</script>



