<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Produto</h5>
            </div>
            <div class="widget-content nopadding">
                <?php echo $custom_error; ?>
                <form action="<?php echo current_url(); ?>" id="formProduto" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="control-group">
                        <?php echo form_hidden('idProdutos',$result->idProdutos) ?>
                        
                    </div>
                    
  <!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"><a style="cursor: pointer;" class="AutoGenerate"><i class="icon-plus-sign-alt"></i> Gerar Codigo</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                          <input class="form-control" type="text" id="produto_code" value="<?=$result->produto_code?>" name="produto_code" placeholder="Codigo do Produto" >
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_code'); ?>
                            </div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Nome do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                         <input class="form-control" type="text" id="produto_nome" value="<?=$result->produto_nome?>" name="produto_nome" placeholder="Nome do Produto" >
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_nome'); ?>
                            </div>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="descricao" class="control-label">Descrição<span class="required">*</span></label>
                        <div class="controls">
                            <textarea class="form-control input-lg" id="product_description"  name="descricao" placeholder="descricao" rows="4" ><?=$result->descricao?></textarea>
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="descricao" class="control-label">Categoria: <span class="required">*</span></label>
                         <div class="controls">    
                            <select class="form-control" name="produto_categoria" id="ver_SubCategoria" >
                                <option value="">Selecione a categoria</option>
                                        <?php
                                        foreach ($categoria_lista as $value)
                                        {
                                            $selected = '';
                                            if($value->idCategoria == $result->categoria_id)
                                            {
                                                $selected = 'selected';
                                            }
                                            ?>
                                        <option <?=$selected?> value="<?=$value->idCategoria?>"><?=$value->categoria_nome?></option>
                                       <?php }  ?>
                            </select>
                         </div>
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_categoria'); ?></div>
                            
                            
                          
                    </div>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
<!a###################################################################################################################################################################################################################################>
         <div class="form-group" >
                        <div id="subcat">
                            <label class="col-md-2 control-label">Sub Categoria<span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control" multiple="" name="sub_categoria_id" >
                                    <option value="">Selecione a Subcategoria</option>
                                      <?php
                                            $sub_categoria_id = $result->sub_categoria_id;
                                             $subcategoria= explode(',',$sub_categoria_id);
                                               foreach ($categoria_lista as $value) {
                                                if($result->categoria_id == $value->idCategoria)
                                                {
                                                    $query = $this->db->query("SELECT * FROM `subcategoria` WHERE parent_categoria_id = '".$value->idCategoria."' ");
                                                    foreach ($query->result() as $user)
                                                    {
                                                        $select ='';
                                                        foreach($subcategoria as $sub)
                                                        {
                                                            if($sub == $user->subcat_id)
                                                            {
                                                                $select = "selected";
                                                            }
                                                        }
                                                           echo '<option value="'.$user->subcat_id.'" '.$select.' >'.$user->subcategoria_nome.'</option>';
                                                    }
                                                }
                                        }?>  

                                </select>
                            </div>
                        </div>
                    </div> 
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Cor do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                           <div class="controls">
                            <select  class="form-control" name="cor_produto"  multiple id="cor_produto"> <span style="color:red">
                             <?php
                                $cor_produto= explode(',',$result->color_id);
                                   foreach ($cor_lista as $value) {
                                    $selcor  = '';
                                    foreach ($cor_produto as $id) {
                                        if($id == $value->id)
                                        {
                                            $selcor = "selected";
                                        }
                                    }?>
                                   <option style="font-size: larger; color:#<?= $value->cor_code?>;" value="<?=$value->id?>"<?=$selcor?> ><?=$value->cor_nome?></option>
                                <?php }
                                ?>
                           </select>
                           </div>   
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Tamanho</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <select   class="form-control" name="tamanho"  multiple id="tamanho">
                               <?php
                            $Tamanho_produto= explode(',',$result->tamanho_id);
                            foreach ($tamanho_lista as $value) {
                                $selcolor  = '';
                                foreach ($Tamanho_produto as $id) {
                                    if($id == $value->id)
                                    {
                                        $selcolor = "selected";
                                    }
                                }?>
                               <option value="<?=$value->id?>"<?=$selcolor?> ><?=$value->tamanho?></option>
                            <?php }
                            ?>
                            </select>
                        </div>   
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label class="control-label"> Destaque do Produto</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="controls">
                            <select class="form-control" name="produto_tipo" >
                              
                            <option value="">Selecione o tipo</option>
                            <option  value="1" <?php if($result->produto_tipo == 1){echo "selected";}?>>Recentes</option>
                            <option  value="2" <?php if($result->produto_tipo == 2){echo "selected";}?>>Popular</option>
                            <option  value="3" <?php if($result->produto_tipo == 3){echo "selected";}?>>Destaque</option>

                                                
                            </select>
                            <div style="margin-top: 0px; color: red;"><?= form_error('produto_tipo'); ?></div>
                        </div>   
                    </div><br>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label  class="control-label"> Produtos Relacionados</a><span class="text-danger">*</span>&nbsp;&nbsp;  </label>
                        <div class="col-md-7">
                                <select  class="form-control" name="produtos_relacionados"  multiple id="produtos_relacionados">
                                <?php
                                    $produtos_relacionado= explode(',',$post->produtos_relacionados);
                                    foreach ($produto_lista as $re)
                                    {
                                        $related_selected = '';
                                        foreach($produtos_relacionado as $related)
                                        {

                                            if($re->idProdutos == $related)
                                            {
                                               $related_selected = 'selected';
                                            }
                                        }
                                ?>
                                    <option value="<?=$re->idProdutos?>"<?=$related_selected?> ><?=$re->produto_nome?></option>
                                   <?php }
                               ?>
                               </select>
                        </div>
                    </div><br>                    
<!a###################################################################################################################################################################################################################################>


                    <div class="control-group">
                        <label class="control-label">Tipo de Movimento</label>
                        <div class="controls">
                            <label for="entrada" class="btn btn-default" style="margin-top: 5px;">Entrada 
                                <input type="checkbox" id="entrada" name="entrada" class="badgebox" value="1" 
                                    <?=($result->entrada == 1)?'checked':''?>>
                                <span class="badge" >&check;</span>
                            </label>
                            <label for="saida" class="btn btn-default" style="margin-top: 5px;">Saída 
                                <input type="checkbox" id="saida" name="saida" class="badgebox" value="1"
                                    <?=($result->saida == 1)?'checked':''?>>
                                <span class="badge" >&check;</span>
                            </label>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="precoCompra" class="control-label">Preço de Compra<span class="required">*</span></label>
                        <div class="controls">
                            <input id="precoCompra" class="money" type="text" name="precoCompra" value="<?php echo $result->precoCompra; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="precoVenda" class="control-label">Preço de Venda<span class="required">*</span></label>
                        <div class="controls">
                            <input id="precoVenda" class="money" type="text" name="precoVenda" value="<?php echo $result->precoVenda; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                    <label for="unidade" class="control-label">Unidade<span class="required">*</span></label>
                    <div class="controls">
                        <select id="unidade" name="unidade">
                            <option value="UN" <?=($result->unidade == 'UN')?'selected':''?>>Unidade</option>
                            <option value="KG" <?=($result->unidade == 'KG')?'selected':''?>>Kilograma</option>
                            <option value="LT" <?=($result->unidade == 'LT')?'selected':''?>>Litro</option>
                            <option value="CX" <?=($result->unidade == 'CX')?'selected':''?>>Caixa</option>
                        </select>                        
                    </div>
                    </div>                    

                    <div class="control-group">
                        <label for="estoque" class="control-label">Estoque<span class="required">*</span></label>
                        <div class="controls">
                            <input id="estoque" type="text" name="estoque" value="<?php echo $result->estoque; ?>"  />
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="estoqueMinimo" class="control-label">Estoque Mínimo</label>
                        <div class="controls">
                            <input id="estoqueMinimo" type="text" name="estoqueMinimo" value="<?php echo $result->estoqueMinimo; ?>"  />
                        </div>
                    </div>
<!a###################################################################################################################################################################################################################################>
                    <div class="control-group">
                        <label for="estoqueMinimo" class="control-label">Capa</label>
                        <div class="controls">
                         <img id="previewimage" onclick="$('#uploadFile').click();" src="<?php echo base_url(); ?>upload/produto/<?php echo $result->idProdutos ?>/<?php echo $result->product_image?>" style="cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;"/>
                          
                         <input type="file" id="uploadFile" name="product_image" style="position: absolute; margin: 0px auto; visibility: hidden;"  />
                         <br><br>
                         <a href="<?php echo base_url().'admin/produtos/galeria/'.$result->idProdutos;?>" class="btn btn-primary" title="Abrir Galeria"><i class="icon-eye-open">Abrir Galeria</i></a>
                                                   </div>
                    </div>       


                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                                <a href="<?php echo base_url() ?>admin/produtos" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>


                </form>
            </div>

         </div>
     </div>
</div>


<script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/js/maskmoney.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".money").maskMoney();

        $('#formProduto').validate({
            rules :{
                  descricao: { required: true},
                  unidade: { required: true},
                  precoCompra: { required: true},
                  precoVenda: { required: true},
                  estoque: { required: true}
            },
            messages:{
                  descricao: { required: 'Campo Requerido.'},
                  unidade: {required: 'Campo Requerido.'},
                  precoCompra: { required: 'Campo Requerido.'},
                  precoVenda: { required: 'Campo Requerido.'},
                  estoque: { required: 'Campo Requerido.'}
            },

            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
                $(element).parents('.control-group').addClass('success');
            }
           });
    });

    
$('document').ready(function()
{ 
	function readURL(input) {
		if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                            $('#previewimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
		}
	}
		$("#uploadFile").change(function(){
			readURL(this);
		});
                
		$(".AutoGenerate").click(function(){
                    
		});
                
        $('.AutoGenerate').bind("click", function() {
            $.post("<?php echo base_url() . 'account/produto/produto-code' ?>", {}, function(data) {
                    $('#produto_code').val(data);
            });
	});
        
        $('#ver_SubCategoria').change(function(event)
        {   
            var categoria_id = $(this).val()
            $.ajax({
			url:'<?=site_url('account/subcategoria_ver')?>',
			data:"categoria_id="+categoria_id,
			type:"post",
			dataType: "json",
			success: function(data) {
				$('#subcat').html(data.html);
			}
		});
        });
        
        
        
        
var quantity_blur=function()
{
    var quantidade = $('#quantidade').val().trim();
    if(quantidade==='' || isNaN(quantidade))
    {
        $('#quantidade').val('1');
        quantidade='1';
    }

    var valor = parseFloat($('#valor').val());
    var bruto = parseFloat(valor * quantidade);
    
    $('#bruto').val(bruto.toFixed(2));
};

var discount_blur=function()
{
    var discount = $('#produto_desconto').val().trim();
    if(discount==='' || isNaN(discount))
    {
        $('#discount').val('0');
        discount='0';
    }
    var bruto = $('#bruto').val();
    var net = bruto - (bruto*discount/100);
    
    $('#net').val(net.toFixed(2));
}

 $('.quantity').blur(quantity_blur);
 $('.discount').blur(discount_blur);
 
 
 
        
       
	
});
</script>







