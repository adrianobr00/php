
            <main id="main-container">
                <div class="content">
                <?php if($this->session->flashdata('sucess')) { ?>
                        <div role="alert" class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                                <strong>Muito Bem!</strong>
                                <?=$this->session->flashdata('sucess')?>
                        </div>
                <?php } ?><a class="btn btn-primary btn-rounded btn-condensed btn-sm" data-toggle="tooltip" title="Add Subcategoria" href="<?=site_url('account/add-subcategoria/'.$this->uri->segment(3))?>"><span title="Add Sub Category"class="icon icon-plus"></span></a>
                                    <div class="widget-box">
                                    <div class="widget-title">
                                    <span class="icon">
                                    <i class="icon-barcode"> </i>
                                    </span>
                                    <h5>SubCAtegorias</h5>

                                    </div>

                                    <div class="widget-content nopadding">


                                    <table class="table table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Sub Categoria Nome</th>
                                        <th>Criado em</th>
                                        <th>Criado Por</th>
                                        <th>Status</th>
                                        <th>Açoes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($subCategoriaLista as $post){
                                    ?>
                                   <tr> 
                                    <td style="text-transform: capitalize;"><?= $post->subcategoria_nome?></td>
                                    <td style="text-transform: capitalize;"><?= $post->criado_data?></td>
                                    <td style="text-transform: capitalize;">
                                        <?php if($post->criado_por == 1)
                                        {
                                            echo "Admin";
                                        }
                                        else
                                        {
                                            echo "Website";
                                        }
                                            ?></td>
                                    <td style="text-transform: capitalize;">
                                        <?php if($post->status == 0)
                                        {
                                            echo "Ativo";
                                        }
                                        else
                                        {
                                            echo "Inativo";
                                        }
                                        ?></td>
                                    <td>
                                       <a class="btn btn-default btn-rounded btn-condensed btn-sm" type="button" data-toggle="tooltip"  title="Edita Sub Categoria" href="<?=site_url('account/edita-subcategoria/'.$post->subcat_id)?>"><i class="icon-pencil"></i></a> 
                                       <a href="<?= base_url() . 'account/sub-categoria-delete/' . $post->subcat_id ?>" data-href="" class="btn btn-danger btn-rounded btn-condensed btn-sm delete"  data-toggle="tooltip" title="Deletar Sub Categoria" ><span class="icon-remove-sign" title="delete"></span></a>
                                    </td>
                                   </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                 

                 
                </div>
               
            </main>
           

<script>
    	$('body').delegate('.editField', 'click', function() {

		var product_id = $(this).attr('data-product_id');
                console.log(product_id);
		var posturl = $(this).attr('data-href');
                console.log(posturl);
		var type = $(this).attr('data-type');
                console.log(type);

		$.ajax({
			type: "POST",
			url: posturl,
			data: {product_id: product_id,type: type},
			dataType: "json",
			success: function(data) {
                            var contact = data['html'];
                            var name = data['name'];
                            
                            $("#header-modal").html("<div class='modal-dialog modal-lg'>"+
                                        "<div class='modal-content'>" +
                                                "<div class='modal-header'>" +
                                                        "<button type='' class='close' data-dismiss='modal' aria-hidden='true'><i class='icons-office-52'></i></button>" +
                                                        "<h4 class='modal-title'><strong>Client</strong></h4>" +
                                                "</div>" +
                                                "<div class='modal-body' id='modal_body'>" +
                                                    "<div class='row text-center'>"+
                                                        "<div class='col-md-2'><label class='control-label'>"+name+"</label></div>"+
                                                        "<div class='col-md-8'>"+contact+"</div>"+
                                                    "</div>"+
                                                "</div>" +
                                                "<div class='modal-footer'> " +
                                                 "<button type='button' class='btn btn-success product_edit_submit_data'>Submit</button>" +
                                                        "<button type='button' class='btn btn-danger  btn-embossed bnt-square' data-dismiss='modal'>Cancle</button>" +
                                                "</div>" +
                                        "</div>"+
                                "</div>"
                            );
                            $('#header-modal').modal('show');
			}
		});
	});
        
        $('#header-modal').delegate('.product_edit_submit_data', 'click', function() {
                var product_value = $('.product_value').val();
                var product_id = $('.product_value').attr('id');
                var posturl = 'product_edit_submit';
                var type = $('.product_value').attr('data-type');
                console.log(type);
                console.log(product_id);
                $.ajax({
			type: "POST",
			url: posturl,
			data: {product_id: product_id,product_value: product_value,type:type},
			dataType: "json",
			success: function(data) {
                            if(data.success)
                            {
                                location.reload();
                            }
			}
		});
        });
        
    </script>