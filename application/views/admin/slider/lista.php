
<span class="col-lg-6" style="text-align: right;">
<a class="btn btn-primary btn-rounded btn-condensed btn-sm" data-toggle="tooltip" title="Adicionar nova Categoria" href="<?=base_url();?>account/add-slider"><span title="Adicionar nova Categoria" >ADICINAR NOVA CATEGORIA</span></a>
</span>	
<?php if($this->session->flashdata('sucess')) { ?>
<div role="alert" class="alert alert-success">
<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
<strong>Muito Bem!</strong>
<?=$this->session->flashdata('sucess')?></div>
<?php } ?>   
<div class="widget-box">
           
     <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
        <h5>Lista de slider</h5>

     </div>

<div class="widget-content nopadding">


<table class="table table-bordered ">
  
                        <div class="block-content">
                            <table class="table table-bordered table-striped js-dataTable-full">
                                <thead>
                                    <tr>
                                          <tr>
                                        <th>Slider Number</th>
                                        <th>Imagems</th>
                                        <th>Criado</th>
                                        <th>Criado Por</th>
                                        <th>Açoes</th>
                                    </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($slider_lista as $value)
                                    {
                                    
                                    ?>
                                   <tr>
                                        <td><?=$value->id?></td>
                                        <td><img height="125px" width="125px" src="<?=base_url();?>upload/slider/<?=$value->id?>/<?=$value->nome;?>"></td>
                                        <td><?=$value->criado_data?></td>
                                        <td><?php 
                                            if(($value->criado_por) == 0)
                                            { 
                                                echo 'Website';
                                            } 
                                            else
                                            { 
                                                echo  "Admin";
                                            }?>
                                        </td>
                                    <td>
                                        <a class="btn btn-danger btn-rounded btn-condensed btn-sm" href="<?=base_url()?>account/slider-delete/<?=$value->id?>" data-href="" data-toggle="tooltip" title="" data-original-title="Delete">
                                            <span class="fa fa-times" title="delete"></span>
                                        </a>
                                    </td>
                                </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                 

                 
                </div>
               
            </main>
           

         

        <!-- Page JS Plugins -->
        <script src="<?= base_url();?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?= base_url();?>assets/js/pages/base_tables_datatables.js"></script>
        
        
        <script src="<?= base_url();?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="<?= base_url();?>assets/js/plugins/bootstrap-datetimepicker/moment.min.js"></script>
        <script src="<?= base_url();?>assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        
        
    </body>
</html>
<script>
    jQuery(function () {
        App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>