<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
<?php if($this->session->flashdata('sucess')) { ?>
<div role="alert" class="alert alert-success">
<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
<strong>Muito Bem!</strong>
<?=$this->session->flashdata('sucess')?></div>
<?php } ?>   
<div class="widget-box">
           
     <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
        <h5>Adicionar nova categoria</h5>

     </div>

<div class="widget-content nopadding">

  <form class="form-horizontal"  method="post" enctype="multipart/form-data">
<div class="form-group">
  <label class="control-label">Nome slider</label>
<div class="controls">
<div class="col-md-7">
                                    <form class="form-horizontal" action="<?=base_url();?>account/add-slider"  method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                             <label class="control-label">Nome em Destaque</label>
                                              <div class="controls">
                                              <div class="col-md-7">
                                              <input class="form-control" type="text" id="product_name" value="<?php echo set_value('slider_nome'); ?>" name="slider_nome" placeholder="Oculos Ryban" >
                                              <div style="margin-top: 0px; color: red;"><?= form_error('slider_nome'); ?></div>
                                              </div>
                                              </div>
                                       <div class="form-group">
                                             <label class="control-label"> Descrição  do slider</label>
                                              <div class="controls">
                                              <div class="col-md-7">
                                              <input class="form-control" type="text" id="product_name" value="<?php echo set_value('descricao'); ?>" name="descricao" placeholder="Descrição do slider" >
                                              <div style="margin-top: 0px; color: red;"><?= form_error('descricao'); ?></div>
                                              </div>
                                              </div>      
                                       <div class="form-group">
                                            <label class="col-md-2 control-label">Slider Image<span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <img id="previewimage" onclick="$('#uploadFile').click();" src="<?php echo base_url(); ?>images/product_image.gif" style="cursor: pointer;height: 210px;width: 210px;position: relative;z-index: 10;"/>
                                                <input type="file" required="" id="uploadFile" name="slider_image" style="position: absolute; margin: 0px auto; visibility: hidden;" accept="image/*" />
                                                <div style="margin-top: 0px; color: red;"><?= form_error('slider_image'); ?></div>
                                            </div>
                                        </div>
                                        
                                       <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                                                <button class="btn btn-sm btn-primary" name="submit" type="submit"><i class="fa fa-check"></i> Add Slider</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- END Main Dashboard Chart -->
                        </div>
                     
                    </div>
                  
                </div>
                <!-- END Page Content -->
            </main>
       
<script>
$('document').ready(function()
{ 
	function readURL(input) {
		if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                            $('#previewimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
		}
	}
		$("#uploadFile").change(function(){
			readURL(this);
		});
	
});
</script>