<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
<?php if($this->session->flashdata('sucess')) { ?>
<div role="alert" class="alert alert-success">
<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
<strong>Muito Bem!</strong>
<?=$this->session->flashdata('sucess')?></div>
<?php } ?>   
<div class="widget-box">
           
     <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
        <h5>Adicionar nova categoria</h5>

     </div>

<div class="widget-content nopadding">

  <form class="form-horizontal"  method="post" enctype="multipart/form-data">
<div class="form-group">
  <label class="control-label">Nome da Categoria</label>
<div class="controls">
<div class="col-md-7">
<input class="form-control" type="text" id="product_name" value="<?php echo set_value('categoria_nome'); ?>" name="categoria_nome" placeholder="Categoria Nome" >
<div style="margin-top: 0px; color: red;"><?= form_error('categoria_nome'); ?></div>
</div>
</div>

<div class="form-actions">
<div class="span12">
<div class="span6 offset3">
<button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Adicionar</button>
<a href="<?php echo base_url() ?>admin/categoria" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
</div>
</div>
</div>