
<span class="col-lg-6" style="text-align: right;">
<a class="btn btn-primary btn-rounded btn-condensed btn-sm" data-toggle="tooltip" title="Adicionar nova Categoria" href="<?=base_url();?>account/categoria-add"><span title="Adicionar nova Categoria" >ADICINAR NOVA CATEGORIA</span></a>
</span>	
<?php if($this->session->flashdata('sucess')) { ?>
<div role="alert" class="alert alert-success">
<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
<strong>Muito Bem!</strong>
<?=$this->session->flashdata('sucess')?></div>
<?php } ?>   
<div class="widget-box">
           
     <div class="widget-title">
        <span class="icon">
            <i class="icon-barcode"></i>
         </span>
        <h5>Lista de Categoria</h5>

     </div>

<div class="widget-content nopadding">


<table class="table table-bordered ">
  
                        <div class="block-content">
                            <table class="table table-bordered table-striped js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>Categoria Nome</th>
                                        <th>Data Criacao</th>
                                        <th>Modificado</th>
                                        <th>Criado por</th>
                                        <th>Status</th>
                                        <th>Açoes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($categoria_lista as $post) 
                                    { ?>
                                    <tr>
                                        <td style="text-transform: capitalize;"><?= $post->categoria_nome?></td>
                                        <td><?= $post->criacao_data?></td>
                                        <td><?= $post->modificado_data?></td>
                                        <td><?php if($post->criado_por == "1")
                                                {
                                                    echo "Admin";
                                                }
                                                else
                                                {
                                                    echo "Website";
                                                }
                                            ?>
                                        </td>
                                        <td><?php if($post->status == "0")
                                                {
                                                    echo "Active";
                                                }
                                                else
                                                {
                                                    echo "Inactive";
                                                }
                                            ?></td>
                                         <td>
                                            <a class="btn btn-default btn-rounded btn-condensed btn-sm" type="button" data-toggle="tooltip"  title="Edit" href="<?=site_url('account/edita-categoria/'.$post->idCategoria)?>"><i class="icon icon-pencil"></i></a>
                                            <a class="btn btn-default btn-rounded btn-condensed btn-sm" data-toggle="tooltip" title="Add Subcategoria" href="<?=site_url('account/add-subcategoria/'.$post->idCategoria)?>"><span title="Add Sub Category"class="icon icon-plus"></span></a>
                                            <a class="btn btn-default btn-rounded btn-condensed btn-sm"  data-toggle="tooltip" title="Ver Sub Categoria" href="<?=site_url('account/ver-subcategoria/'.$post->idCategoria)?>"><span title="Ver Sub Categoria"class="icon-eye-open"></span></a>
                                            <?php if ($post->status == '1') { ?>
                                            <a href="<?= base_url() . 'account/categoria-status/'.$post->idCategoria.'/0'?>" data-toggle="tooltip" title="Inactive" class="btn btn-danger btn-rounded btn-condensed btn-sm changestatus"><span  class="icon icon-off" title="Inactive"></span></a>
                                            <?php } else { ?>
                                            <a href="<?= base_url() . 'account/categoria-status/'.$post->idCategoria.'/1'?>" data-toggle="tooltip" title="Active" class="btn btn-success btn-rounded btn-condensed btn-sm changestatus"><span class="icon icon-ok-sign" title="Active"></span></a>
                                        <?php } ?>
                                            <a href="<?= base_url() . 'account/categoria-delete/' . $post->idCategoria ?>" data-href="" class="btn btn-danger btn-rounded btn-condensed btn-sm delete"  data-toggle="tooltip" title="Delete" ><span class="icon icon-remove-sign" title="delete"></span></a>
                                    </td>
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                 

                 
                </div>
               
            </main>
           

         
        </div>
           <div class="modal fade" id="header-modal" aria-hidden="true"></div>
      
        
    </body>
</html>
<script>
    	$('body').delegate('.editField', 'click', function() {

		var product_id = $(this).attr('data-product_id');
                console.log(product_id);
		var posturl = $(this).attr('data-href');
                console.log(posturl);
		var type = $(this).attr('data-type');
                console.log(type);

		$.ajax({
			type: "POST",
			url: posturl,
			data: {product_id: product_id,type: type},
			dataType: "json",
			success: function(data) {
                            var contact = data['html'];
                            var name = data['name'];
                            
                            $("#header-modal").html("<div class='modal-dialog modal-lg'>"+
                                        "<div class='modal-content'>" +
                                                "<div class='modal-header'>" +
                                                        "<button type='' class='close' data-dismiss='modal' aria-hidden='true'><i class='icons-office-52'></i></button>" +
                                                        "<h4 class='modal-title'><strong>Client</strong></h4>" +
                                                "</div>" +
                                                "<div class='modal-body' id='modal_body'>" +
                                                    "<div class='row text-center'>"+
                                                        "<div class='col-md-2'><label class='control-label'>"+name+"</label></div>"+
                                                        "<div class='col-md-8'>"+contact+"</div>"+
                                                    "</div>"+
                                                "</div>" +
                                                "<div class='modal-footer'> " +
                                                 "<button type='button' class='btn btn-success product_edit_submit_data'>Submit</button>" +
                                                        "<button type='button' class='btn btn-danger  btn-embossed bnt-square' data-dismiss='modal'>Cancle</button>" +
                                                "</div>" +
                                        "</div>"+
                                "</div>"
                            );
                            $('#header-modal').modal('show');
			}
		});
	});
        
        $('#header-modal').delegate('.product_edit_submit_data', 'click', function() {
                var product_value = $('.product_value').val();
                var product_id = $('.product_value').attr('id');
                var posturl = 'product_edit_submit';
                var type = $('.product_value').attr('data-type');
                console.log(type);
                console.log(product_id);
                $.ajax({
			type: "POST",
			url: posturl,
			data: {product_id: product_id,product_value: product_value,type:type},
			dataType: "json",
			success: function(data) {
                            if(data.success)
                            {
                                location.reload();
                            }
			}
		});
        });
        
    </script>