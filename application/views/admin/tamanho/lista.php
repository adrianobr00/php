
<span class="col-lg-6" style="text-align: right;">
            <a class="btn btn-primary btn-rounded btn-condensed btn-sm" data-toggle="tooltip" title="Adicionar nova Categoria" href="<?=base_url();?>account/tamanho-add"><span title="Adicionar nova Categoria" >ADICINAR TAMANHO</span></a>
            </span>	
                    <?php if($this->session->flashdata('sucess')) { ?>
                        <div role="alert" class="alert alert-success">
                            <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
                                <strong>Muito Bem!</strong>
                                    <?=$this->session->flashdata('sucess')?></div>
                                    <?php } ?>   
                                    <div class="widget-box">

                                    <div class="widget-title">
                                    <span class="icon">
                                    <i class="icon-barcode"></i>
                                    </span>
                                <h5>Lista de Categoria</h5>

                            </div>

                    <div class="widget-content nopadding">


<table class="table table-bordered ">

            <div class="block-content">
            <table class="table table-bordered table-striped js-dataTable-full">
                    <thead>
                    <tr>
                        <th>Tamanho Nome</th>
                        <th>Data Criacao</th>
                        <th>Modificado</th>
                        <th>Criado por</th>
                        <th>Status</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tamanho_lista as $post) 
                    { ?>
                    <tr>
                        <td style="text-transform: capitalize;"><?= $post->tamanho;?></td>
                        <td><?= $post->criado_data?></td>
                        <td><?= $post->modificado_data?></td>
                        <td><?php if($post->criado_por == "1")
                                {
                                    echo "Admin";
                                }
                                else
                                {
                                    echo "Website";
                                }
                            ?>
                        </td>
                          <td><?php if($post->status == "active")
                                {
                                    echo '<li class="btn btn-success">Ativo</li>';
                                }
                                else
                                {
                                    echo '<li class="btn btn-danger">Inativo</li>';
                                }
                            ?></td>
                         <td>
                            <a class="btn btn-default btn-rounded btn-condensed btn-sm" type="button" data-toggle="tooltip"  title="Edit" href="<?=site_url('account/tamanho-edita/'.$post->id)?>"><i class="icon-pencil"></i></a>
                            <a href="<?= base_url() . 'account/tamanho-delete/' . $post->id ?>" data-href="" class="btn btn-danger btn-rounded btn-condensed btn-sm delete"  data-toggle="tooltip" title="Delete" ><span class="icon-remove" title="delete"></span></a>
                        </td>

        </tr>
        <?php } ?>
        </tbody>
</table>
</div>
</div>



</div>

</main>


