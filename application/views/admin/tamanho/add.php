<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
opacity: 0;
}

.badgebox + .badge
{
/* Move the check mark away when unchecked */
text-indent: -999999px;
/* Makes the badge's width stay the same checked and unchecked */
width: 27px;
}

.badgebox:focus + .badge
{
/* Set something to make the badge looks focused */
/* This really depends on the application, in my case it was: */

/* Adding a light border */
box-shadow: inset 0px 0px 5px;
/* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
/* Move the check mark back when checked */
text-indent: 0;
}
</style>
<?php if($this->session->flashdata('sucess')) { ?>
<div role="alert" class="alert alert-success">
<button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Fechar</span></button>
<strong>Muito Bem!</strong>
<?=$this->session->flashdata('sucess')?></div>
<?php } ?>   
<div class="widget-box">

<div class="widget-title">
<span class="icon">
<i class="icon-barcode"></i>
</span>
<h5>Adicionar Tamanho</h5>

</div>

<div class="widget-content nopadding">

<form class="form-horizontal"  method="post" enctype="multipart/form-data">
<div class="form-group">
<label class="control-label">Nome do tamanho</label>
<div class="controls">
<div class="col-md-7">
                <input class="form-control" type="text" id="tamanho_nome" value="<?php echo set_value('tamanho_nome'); ?>" name="tamanho_nome" placeholder="Nome do Tamanho" >
                 <div style="margin-top: 0px; color: red;"><?= form_error('tamanho_nome'); ?></div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
                <button class="btn btn-sm btn-primary" name="submit" type="submit"><i class="icon icon-check"></i> Adicionar</button>
                <a href="<?php echo base_url() ?>admin/categoria" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
            </div>
        </div>
    </form>


<script>
$('document').ready(function()
{ 
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onload = function (e) {
$('#previewimage').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#uploadFile").change(function(){
readURL(this);
});

});
</script>