<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
             <?php if($this->session->flashdata('sucess')) { ?>
                                   <div role="alert" class="alert alert-success">
                                           <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                                           <strong>Muito Bem!</strong>
                                           <?=$this->session->flashdata('sucess')?>
                                   </div>
                           <?php } ?>
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Editar tamanho</h5>
            </div>
            <div class="widget-content nopadding">
               <div class="control-group">
                                    <?php
                                    foreach ($editartamanho as $post)
                                    {
                                    ?>
                                      <form class="form-horizontal"  method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                              <div class="control-group">
                        <label class="control-label">Nome da Categoria</label>
                        <div class="controls">
                                            <div class="col-md-7">
                                                 <input class="form-control" type="hidden" id="tamanho_id" value="<?=$post->id;?>" name="tamanho_id">
                                                <input class="form-control" type="text" id="tamanho_nome" value="<?=$post->tamanho;?>" name="tamanho_nome" placeholder="Nome" >
                                              
                                            </div>
                                        </div>
                                        
                                      <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Alterar</button>
                                <a href="<?php echo base_url() ?>admin/tamanho" id="" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                            </div>
                        </div>
                    </div>
                                    </form>
                                    <?php }?>
                                </div>
                            </div>
                            <!-- END Main Dashboard Chart -->
                        </div>
                     
                    </div>
                  
                </div>
                <!-- END Page Content -->
            </main>
   
        </div>
    
       
        
    </body>
</html>
<script>
$('document').ready(function()
{ 
	function readURL(input) {
		if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                            $('#previewimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
		}
	}
		$("#uploadFile").change(function(){
			readURL(this);
		});
	
});
</script>