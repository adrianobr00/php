<style>
/* Hiding the checkbox, but allowing it to be focused */
.badgebox
{
    opacity: 0;
}

.badgebox + .badge
{
    /* Move the check mark away when unchecked */
    text-indent: -999999px;
    /* Makes the badge's width stay the same checked and unchecked */
	width: 27px;
}

.badgebox:focus + .badge
{
    /* Set something to make the badge looks focused */
    /* This really depends on the application, in my case it was: */
    
    /* Adding a light border */
    box-shadow: inset 0px 0px 5px;
    /* Taking the difference out of the padding */
}

.badgebox:checked + .badge
{
    /* Move the check mark back when checked */
	text-indent: 0;
}
</style>
<div class="row-fluid" style="margin-top:0">
    <div class="span12">
        <div class="widget-box">
             <?php if($this->session->flashdata('sucess')) { ?>
                                   <div role="alert" class="alert alert-success">
                                           <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
                                           <strong>Muito Bem!</strong>
                                           <?=$this->session->flashdata('sucess')?>
                                   </div>
                           <?php } ?>
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-align-justify"></i>
                </span>
                <h5>Editar Cor</h5>
            </div>
            <div class="widget-content nopadding">
               <div class="control-group">
                                    <?php
                                    foreach ($editar_Cor as $post)
                                    {
                                    ?>
                                       <form class="form-horizontal"  method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Cor Nome<span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <input class="form-control" type="hidden" id="cor_id" value="<?=$post->id;?>" name="cor_id">
                                                <input class="form-control" type="text" id="cor_nome" value="<?=$post->cor_nome;?>" name="cor_nome" placeholder="Nome da Cor" >
                                                 <div style="margin-top: 0px; color: red;"><?= form_error('cor_nome'); ?></div>
                                            </div>
                                        </div>
                                          
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Codigo<span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <input class="form-control jscolor jscolor-active" onclick="$(this).addClass('jscolor');" type="text" autocomplete="off" id="cor_code" value="<?=$post->cor_code;?>" name="cor_code" >
                                                 <div style="margin-top: 0px; color: red;"><?= form_error('cor_code'); ?></div>
                                            </div>
                                        </div>
                                       
                                        
                                       <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                                                <button class="btn btn-sm btn-primary" name="submit" type="submit"><i class="fa fa-check"></i> Update Color</button>
                                            </div>
                                        </div>
                                    </form>
                                    <?php }?>
                                </div>
                            </div>
                            <!-- END Main Dashboard Chart -->
                        </div>
                     
                    </div>
                  
                </div>
                <!-- END Page Content -->
            </main>
      
        
    </body>
</html>
<script>
$('document').ready(function()
{ 
	function readURL(input) {
		if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                            $('#previewimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
		}
	}
		$("#uploadFile").change(function(){
			readURL(this);
		});
	
});
</script>