
<div id="sidebar"> <a href="#" class="visible-phone"><i class="icon icon-list"></i> Menu</a>
  <ul>


    <li class="<?php if(isset($menuPainel)){echo 'active';};?>"><a href="<?php echo base_url('admin')?>"><i class="icon icon-home"></i> <span>Painel</span></a></li>
    
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vCliente')){ ?>
        <li class="<?php if(isset($menuClientes)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/clientes"><i class="icon icon-group"></i> <span>Clientes</span></a></li>
    <?php } ?>
        
   <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){ ?>
        <li class="<?php if(isset($menuProdutos)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/produtos"><i class="icon icon-barcode"></i> <span>Produtos</span></a></li>
        <li class="<?php if(isset($menuProdutos)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/marca"><i class="icon icon-star-half"></i> <span>Marcas</span></a></li>
    <?php } ?>
        
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vCategoria')){ ?>
        <li class="<?php if(isset($menuCategoria)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/categoria"><i class="icon icon-plus"></i> <span>Categorias</span></a></li>
    <?php } ?>
        
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vCor')){ ?>
        <li class="<?php if(isset($menuCor)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/cor"><i class="icon icon-pencil"></i> <span>Cor</span></a></li>
    <?php } ?> 
        
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vTamanho')){ ?>
        <li class="<?php if(isset($menuTamanho)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/tamanho"><i class="icon icon-text-height"></i> <span>Tamanho</span></a></li>
    <?php } ?>    
    
     
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vServico')){ ?>
        <li class="<?php if(isset($menuServicos)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/servicos"><i class="icon icon-wrench"></i> <span>Serviços</span></a></li>
    <?php } ?>

    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vOs')){ ?>
        <li class="<?php if(isset($menuOs)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/os"><i class="icon icon-tags"></i> <span>Ordens de Serviço</span></a></li>
    <?php } ?>

    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vVenda')){ ?>
        <li class="<?php if(isset($menuVendas)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/vendas"><i class="icon icon-shopping-cart"></i> <span>Vendas</span></a></li>
    <?php } ?>
    
    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vArquivo')){ ?>
        <li class="<?php if(isset($menuArquivos)){echo 'active';};?>"><a href="<?php echo base_url()?>admin/arquivos"><i class="icon icon-hdd"></i> <span>Arquivos</span></a></li>
    <?php } ?>

    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'vLancamento')){ ?>
        <li class="submenu <?php if(isset($menuFinanceiro)){echo 'active open';};?>">
          <a href="#"><i class="icon icon-money"></i> <span>Financeiro</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
          <ul>
            <li><a href="<?php echo base_url()?>admin/financeiro/lancamentos">Lançamentos</a></li>
          </ul>
        </li>
    <?php } ?>

    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rCliente') || $this->permission->checkPermission($this->session->userdata('permissao'),'rProduto') || $this->permission->checkPermission($this->session->userdata('permissao'),'rServico') || $this->permission->checkPermission($this->session->userdata('permissao'),'rOs') || $this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro') || $this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){ ?>
        
        <li class="submenu <?php if(isset($menuRelatorios)){echo 'active open';};?>" >
          <a href="#"><i class="icon icon-list-alt"></i> <span>Relatórios</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
          <ul>

            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rCliente')){ ?>
                <li><a href="<?php echo base_url()?>admin/relatorios/clientes">Clientes</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rProduto')){ ?>
                <li><a href="<?php echo base_url()?>admin/relatorios/produtos">Produtos</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rServico')){ ?>
                <li><a href="<?php echo base_url()?>admin/relatorios/servicos">Serviços</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rOs')){ ?>
                 <li><a href="<?php echo base_url()?>admin/relatorios/os">Ordens de Serviço</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rVenda')){ ?>
                <li><a href="<?php echo base_url()?>admin/relatorios/vendas">Vendas</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'rFinanceiro')){ ?>
                <li><a href="<?php echo base_url()?>admin/relatorios/financeiro">Financeiro</a></li>
            <?php } ?>
            
          </ul>
        </li>

    <?php } ?>

    <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cUsuario')  || $this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente') || $this->permission->checkPermission($this->session->userdata('permissao'),'cPermissao') || $this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){ ?>
        <li class="submenu <?php if(isset($menuConfiguracoes)){echo 'active open';};?>">
          <a href="#"><i class="icon icon-cog"></i> <span>Configurações</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
          <ul>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cUsuario')){ ?>
                <li><a href="<?php echo base_url()?>admin/usuarios">Usuários</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cEmitente')){ ?>
                <li><a href="<?php echo base_url()?>admin/dashboard/emitente">Emitente</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cPermissao')){ ?>
                <li><a href="<?php echo base_url()?>admin/permissoes">Permissões</a></li>
            <?php } ?>
            <?php if($this->permission->checkPermission($this->session->userdata('permissao'),'cBackup')){ ?>
                <li><a href="<?php echo base_url()?>admin/dashboard/backup">Backup</a></li>
            <?php } ?>
            
            <?php if(!$this->permission->checkPermission($this->session->userdata('permissao'),'cSlider')){ ?>
                <li><a href="<?php echo base_url()?>admin/Slider">Slider</a></li>
            <?php } ?>    
 
          </ul>
        </li>
    <?php } ?>
    
    
  </ul>
</div>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="<?php  base_url('admin')?>" title="Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a> <?php if($this->uri->segment(1) != null){?><a href="<?php echo base_url().'admin/'.$this->uri->segment(1)?>" class="tip-bottom" title="<?php echo ucfirst($this->uri->segment(1));?>"><?php echo ucfirst($this->uri->segment(1));?></a> <?php if($this->uri->segment(2) != null){?><a href="<?php echo base_url().'admin/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3) ?>" class="current tip-bottom" title="<?php echo ucfirst($this->uri->segment(2)); ?>"><?php echo ucfirst($this->uri->segment(2));} ?></a> <?php }?></div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
          <?php if($this->session->flashdata('error') != null){?>
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <?php echo $this->session->flashdata('error');?>
                           </div>
                      <?php }?>

                      <?php if($this->session->flashdata('success') != null){?>
                            <div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <?php echo $this->session->flashdata('success');?>
                           </div>
                      <?php }?>
                          
                      <?php if(isset($view)){echo $this->load->view($view, null, true);}?>


      </div>
    </div>
  </div>
</div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> <a href="" target="_blank"><?php echo date('Y'); ?> &copy; Adriano Alves </a></div>
</div>
<!--end-Footer-part-->


<script src="<?php echo base_url();?>assets/admin/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/matrix.js"></script> 
<script src="<?php echo base_url(); ?>assets/admin/js/core/jquery.placeholder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/core/js.cookie.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/jscolor.js"></script>

</body>
</html>

