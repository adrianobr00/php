
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
      
    <li class=""><a title="" href="<?php echo site_url();?>/admin/dashboard/minhaConta"><i class="icon icon-star"></i> <span class="text">Minha Conta</span></a></li>
    <li class=""><a title="" href="<?php echo site_url();?>admin/mine"><i class="icon icon-eye-open"></i> <span class="text">Área do Cliente</span></a></li>
    <li class="pull-right"><a href="" target="_blank"><i class="icon icon-asterisk"></i> <span class="text">Versão: <?php echo $this->config->item('app_version'); ?></span></a></li>
    <li class=""><a title="" href="<?php echo site_url();?>/admin/dashboard/sair"><i class="icon icon-share-alt"></i> <span class="text">Sair do Sistema</span></a></li>
    
  </ul>

</div>


<!--start-top-serch-->
<div id="search">
  <form action="<?php echo base_url()?>admin/dashboard/pesquisar">
    <input type="text" name="termo" placeholder="Pesquisar..."/>
    <button type="submit"  class="tip-bottom" title="Pesquisar"><i class="icon-search icon-white"></i></button>
    
  </form>
</div>

<!--close-top-serch--> 
