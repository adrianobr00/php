
	<!--header-->
        	   <div class="search-box">
					<div id="sb-search" class="sb-search">
						<form>
							<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"> </span>
						</form>
					</div>
				</div>
			
<!-- search-scripts -->
					<script src="<?=base_url();?>assets/js/classie.js"></script>
					<script src="<?=base_url();?>assets/js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
					<!-- //search-scripts -->
					<div class="clearfix"></div>
					</div>
				
			
       
	
			<!--header-->
		<div class="banner-section">
			<div class="container">
				<div class="banner-grids">
					<div class="col-md-6 banner-grid">
						<h2>the latest collections</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a href="<?=base_url();?>" class="button"> shop now </a>
					</div>
				<div class="col-md-6 banner-grid1">
						<img src="<?=base_url();?>images/p2.png" class="img-responsive" alt=""/>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</div>
		<div class="banner-bottom">
		<div class="gallery-cursual">
		<!--requried-jsfiles-for owl-->
		
		<!--requried-jsfiles-for owl -->
		<!--start content-slider-->
		<div id="owl-demo" class="owl-carousel text-center">
                    <?php   foreach ($slider as $slide)
                    {?>
                    <div class="item" >
                            <img class="lazyOwl" data-src="<?= base_url();?>upload/slider/<?php echo $slide->id?>/<?php echo $slide->nome?>" style="height: 220px;" alt="name">
			</div>
                     <?php }
                    ?>
		</div>
		<!--sreen-gallery-cursual-->
		</div>
		</div>
		<div class="gallery">
                <div class="container">
			<h3>Popular Products</h3>
			<div class="gallery-grids">
                            <?php 
                                if(!empty($produtos_populares))
                                {
                                    foreach ($produtos_populares as $popular)
                                    { 
                                        
                                        $id = $popular->idProdutos;
                                        $name = $popular->produto_nome;
                                        $description = $popular->descricao;
                                        $price = $popular->precoVenda;
                                        $image = $popular->product_image;
                                    ?>
				<div class="col-md-3 gallery-grid ">
                                    <a><img class="quicklook" id="<?=$popular->idProdutos;?>" src="<?=base_url();?>/upload/produto/<?=$popular->idProdutos;?>/<?=$popular->product_image;?>" style="height: 310px; width: 99%; cursor: pointer;" alt=""/>
					<div class="gallery-info quicklook" id="<?=$popular->idProdutos;?>" style="cursor: pointer;">
					<div class="quick">
                                            <p><span class="glyphicon glyphicon-eye-open "  style="cursor: pointer;" aria-hidden="true"></span> view</p>
					</div>
					</div>
                                    </a>
					<div class="galy-info">
						<p><?=$popular->produto_nome;?></p>
						<div class="galry">
						<div class="prices">
                                                    <h5  class="item_price" style="color: #333333;">Rs. <?=$popular->precoVenda;?></h5>
						</div>
					<div class="clearfix"></div>
					</div>
					</div>
				</div>
                             <?php }
                                }
                                else
                                {
                                     echo "No Item available!!";
                                } ?>
				
			</div>
		</div>
		</div>
                        
                        
            <div class="gallery">
                <div class="container">
			<h3>Produtos Recentes</h3>
			<div class="gallery-grids">
                              <?php 
                                    if(!empty($produtos_recentes))
                                    {
                                    foreach ($produtos_recentes as $latest)
                                    { 
                                                                $id = $latest->idProdutos;
                                                                $name = $latest->produto_nome;
                                                                $description = $latest->descricao;
                                                                $price = $latest->precoVenda;
                                                                $image = $latest->product_image;
                                    ?>
				<div class="col-md-3 gallery-grid ">
                                    <a><img class="quicklook" id="<?=$latest->idProdutos;?>" src="<?=base_url();?>/upload/produto/<?=$latest->idProdutos;?>/<?=$latest->product_image;?>" style="height: 310px; width: 99%; cursor: pointer;" alt=""/>
					<div class="gallery-info quicklook" id="<?=$latest->idProdutos;?>" style="cursor: pointer;">
					<div class="quick">
                                            <p><span class="glyphicon glyphicon-eye-open "  style="cursor: pointer;" aria-hidden="true"></span> view</p>
					</div>
					</div>
                                    </a>
					<div class="galy-info">
						<p><?=$latest->produto_nome;?></p>
						<div class="galry">
						<div class="prices">
						<h5  class="item_price" style="color: #333333;">Rs. <?=$latest->precoVenda;?></h5>
						</div>
					
					</div>
					</div>
				</div>
                             <?php }
                                }
                                else
                                {
                                     echo "No Item available!!";
                                } ?>
				
			</div>
		</div>
		</div>
                    
                        
            <div class="gallery">
                <div class="container">
			<h3>Produtos emm destaque</h3>
			<div class="gallery-grids">
                                <?php 
                                 if(!empty($produtos_destaque))
                                    {
                                    foreach ($produtos_destaque as $feature)
                                       { 
                                                                $id = $latest->idProdutos;
                                                                $name = $latest->produto_nome;
                                                                $description = $latest->descricao;
                                                                $price = $latest->precoVenda;
                                                                $image = $latest->product_image;
                                    ?>
				<div class="col-md-3 gallery-grid ">
                                     <a><img class="quicklook " id="<?=$feature->idProdutos;?>" src="<?=base_url();?>/upload/produto/<?=$feature->idProdutos;?>/<?=$feature->product_image;?>" style="height: 310px; width: 99%; cursor: pointer;" alt=""/>
					<div class="gallery-info quicklook" id="<?=$feature->idProdutos;?>" style="cursor: pointer;">
					<div class="quick">
                                            <p><span class="glyphicon glyphicon-eye-open "  style="cursor: pointer;" aria-hidden="true"></span> view</p>
					</div>
					</div>
                                    </a>
					<div class="galy-info">
						<p><?=$feature->produto_nome;?></p>
						<div class="galry">
						<div class="prices">
						<h5 class="item_price" style="color: #333333;">Rs. <?=$feature->precoVenda;?></h5>
						</div>
					<div class="clearfix"></div>
					</div>
					</div>
				</div>
                             <?php }
                                }
                                else
                                {
                                     echo "No Item available!!";
                                } ?>
				
			</div>
		</div>
		</div>
                        
	
                        <div class="modal fade" id="header-modal" aria-hidden="true"></div>
	<!--footer-->
		
	<!--footer-->
	<script src="<?=base_url();?>assets/js/owl.carousel.js"></script>
        <script>
                $(document).ready(function() {
                        $("#owl-demo").owlCarousel({
                                items : 5,
                                lazyLoad : true,
                                autoPlay : true,
                                pagination : false,
                        });
                });
        </script>	
</body>
</html>

<script type="text/javascript">
            	$(document).ready(function() {
                    
                                 $("#header-modal").delegate("#addcartform","submit",function(e){       
		var color = document.forms["produtoformcart"]["color"].value;
		if (color == null || color == "") {
			alert("Color must be Selected. Click for select.");
			return false;
		}
		
		var size = document.forms["produtoformcart"]["size"].value;
		if (size == null || size == "") {
			alert("Size must be Selected. Click for select.");
			return false;
		}
            });
                    
                     
                         
                   
                     $("#header-modal").delegate(".data_values","click",function(e){
                        var id = $(this).attr('id');
                        $('.data_values').removeClass('active');
                        $("#"+id).addClass('active');
                        $("#colorProduct").val(id.slice(1));
                    });
                     $("#header-modal").delegate(".data_values_size","click",function(e){
                        var id = $(this).attr('id');
                        $('.data_values_size').removeClass('active');
                        $("#"+id).addClass('active');
                        $("#sizeProduct").val(id.slice(4));
                    });

                    
                    $('.quicklook').click(function() {
                        var produto_id = $(this).attr('id');
                        $.ajax({
                                type: "POST",
                                url: "<?=base_url();?>produto/SingleProuctDetail",
                                data: {produto_id: produto_id},
                                dataType: "json",
                                success: function(data) {
                                     $("#header-modal").html(data.success);
                                      $('#header-modal').modal('show');  
                                }
                               
                        });
                });
        });
	</script>