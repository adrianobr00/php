<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*

 * clase para listar cores editar e excluir
 * criado por Adriano Alves
 *  */
class Marca_Model extends CI_Model {
    /*funcao para listar cor*/
        public function get_lista()
        {
                $this->db->select(" * ");
                $this->db->from('marcas');
//                $this->db->where('status','0');
                $query = $this->db->get();
                return $query->result();
        }
        
        /*funcao para pegar marca por id*/
        public function ver_Marca($id)
        {
                $this->db->select(" * ");
                $this->db->from('marcas');
                $this->db->where('idMarcas',$id);
                $query = $this->db->get();
                return $query->result();
        }
        /*funcao para adicionar nova cor*/
         function add_Marca($data){
        $this->db->insert('marcas', $data);         
        if ($this->db->affected_rows() == '1')
		{
	    $response=$this->db->insert_id();
            return $response;    
		}
                
		
		return FALSE;       
    }
        /*funcao para enviar e editar no banco a cor*/
         public function editar_Marca($data,$id)
        {
           $this->db->where('idMarcas',$id);
           $query = $this->db->update('marcas',$data);
        }
        
        /*FUNCAO PARA ATUALIZAR IMAGEM DA SUBCATEGORIA*/	
    function update_Image($id, $subfilename) {
        $this->db->set('logo', $subfilename);
        $this->db->where('idMarcas',$id);
        $query = $this->db->update('marcas');
    }
        /*funcao para deletar cor*/
        function deletar_marca($id) {
           $this->db->where('idMarcas',$id);
           $this->db->delete('marcas');
          if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}else{
		
		return FALSE;        
    }

}}
