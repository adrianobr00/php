<?php
class Galeria_model extends CI_Model {

    /**
     * author: Adriano ALves
     * email: adrianobr00@gmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
        	$this->tablename = $this->session->userdata('table_id').'fotos';
		$this->tablesubcategoria = $this->session->userdata('table_id').'subcategoria';
		$this->tablecategoria = $this->session->userdata('table_id').'categoria';
        }
    function get($table,$fields,$idProduto,$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idFotos','desc');
        $this->db->limit($perpage,$start);
        if($idProduto){
        $this->db->where('idProdutos',$idProduto);
        }
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    
        function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
	    $response=$this->db->insert_id();
            return $response;    
		}
            return FALSE;       
    }
    
    /*FUNCAO PARA ATUALIZAR ENDEREÇO DA IMAGEN NO BANCO DE DADOS*/
        function update_Imagem($inserir_id, $mainimagefilename) {
            $this->db->set('product_image', $mainimagefilename);
            $this->db->where('idProdutos',$inserir_id);
            $query = $this->db->update('produtos');
            
        }

 function atualizarCapa($foto_album)
{   $foto_album = $_POST['foto_album'];
     $foto_id = $_POST['foto_id'];
     $this->db->query( "update fotos set foto_pos = 1 where foto_album = $foto_album" );
     $this->db->query( "update fotos set foto_pos = 0 where foto_id = $foto_id" );
       if ($this->db->query())
		{
	   
            return TRUE;    
		}
                
		
		return FALSE;       
    }
        
    

function atualizarNomeFoto()
{
    if ( isset( $_POST['foto_caption'] ) )
    {
        $foto_caption = utf8_decode( fulltrim( $_POST['foto_caption'] ) );
        //$foto_info = utf8_decode( fulltrim( $_POST['foto_info'] ) );
        $foto_id = preg_replace( '/f\_/', '', fulltrim( $_POST['foto_id'] ) );
        if ( $foto_caption != "")
        {
            $db = new Mysql;
            //$db->query( "update fotos set foto_caption = '$foto_caption', foto_info = '$foto_info' where foto_id = $foto_id" );
            $db->query( "update fotos set foto_caption = '$foto_caption' where foto_id = $foto_id" );
            @header( "Content-Type: text/html; charset=ISO-8859-1", true );
            if ( $foto_caption != "" )
            {
                echo 'Foto Atualizada<Br/>' . $foto_caption;
            }
        }
        else
        {
            echo 1;
        }
    }
}

function deleteFoto($fieldID,$ID){
    $this->db->select(" * ");
     $this->db->from('fotos');
     $this->db->where('idFotos',$ID);
     $query = $this->db->get();
    if ( $query->result() )
    {
     $this->db->where('idFotos',$ID);;
    $this->db->delete('fotos');}
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }

}
