<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*classe do tamanho editar listar criar e exculir

 * CRIADO POR ADRIANO ALVES
 *  */
class Tamanho_Model extends CI_Model {
   /*FUNCAO PARA LISTA TAMANHOS*/ 
        public function get_Lista()
        {
                $this->db->select(" * ");
                $this->db->from('tamanho');
//                $this->db->where('status','0');
                $query = $this->db->get();
                return $query->result();
        }
        /*FUNCAO PARA SELECIONA TAMANHO PARA EDITAR */
        public function editar_TamanhoSelecionado($id)
        {
                $this->db->select(" * ");
                $this->db->from('tamanho');
                $this->db->where('id',$id);
                $query = $this->db->get();
                return $query->result();
        }
        /*FUNCAO CRIAR*/
        public function add_Tamanho($data)
        {
             $this->db->insert('tamanho', $data); 
             return TRUE;
        }
        /*FUNCAO PARA EDITAR*/
        public function edita_TamanhoEnviar($data,$categoria_id)
        {
           $this->db->where('id',$categoria_id);
           $query = $this->db->update('tamanho',$data);
        }
        /*FUNCAO PARA DELETAR TAMANHO*/
        function deletar_Tamanho($id) {
            $this->db->where('id',$id);
            $this->db->delete('Tamanho');
        }
        
}