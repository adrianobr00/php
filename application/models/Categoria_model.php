<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* classe para edicao criacao e exclusao de categoria

 * 
 * 
 *  */
class Categoria_Model extends CI_Model {
    /*funcao para receber categorias cadastrados no sistema*/
        public function get_Lista()
        {
                $this->db->select(" * ");
                $this->db->from('categoria');
//                $this->db->where('status','0');
                $query = $this->db->get();
                return $query->result();
        }
/* tabela para receber tamanhos cadastrados*/
        public function get_ListaTamanho()
        {
                $this->db->select(" * ");
                $this->db->from('tamanho');
                $query = $this->db->get();
                return $query->result();
        }
        /* tabela para receber cores cadastrados*/
        public function get_Lista_Cor()
        {
                $this->db->select(" * ");
                $this->db->from('color');
                $query = $this->db->get();
                return $query->result();
        }
        /*funcao para receber a categoria por id para editar categoria*/
        public function editar_Categoria($id)
        {
                $this->db->select(" * ");
                $this->db->from('categoria');
                $this->db->where('idcategoria',$id);
                $query = $this->db->get();
                return $query->result();
        }
        /*funcao para criar categoria*/
        public function Categoria_Add($data)
        {
             $this->db->insert('categoria', $data); 
             return TRUE;
        }
        /*funcao para editar categoria*/
        public function editar_CategoriaEnviar($data,$categoria_id)
        {
           $this->db->where('idcategoria',$categoria_id);
           $query = $this->db->update('categoria',$data);
        }
        /*funcao para deletar categoria e subcategoria cadastrados*/
        function deletar_Categoria($id) {
            $this->db->where('idcategoria',$id);
            $this->db->delete('categoria');
            $this->db->where('parent_categoria_id',$id);
            $this->db->delete('subcategoria');
        }
        /*alterar status da categoria para ativo ou inativo*/
        function categoria_Status($id,$status) {
            $this->db->set('status', $status);
            $this->db->where('idcategoria',$id);
            $query = $this->db->update('categoria');
        }
        
}