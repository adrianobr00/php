<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*classe para pegar produtos para mostrar no site
para tambem a criacao de contatos para mensagens
 * slides,produtos recentes,populares, e tambem pega os detalhes do produto
 * 
 * 
 * criado por Adriano Alves
 *  */
class Dashboard_Model extends CI_Model {
    /*funcao para pegar produtos recentes*/
        public function produtos_recentes()
        {
            $this->db->select(" * ");
            $this->db->from('produtos');
            $this->db->where('produto_tipo', '1');
            $this->db->limit('4');
            $query = $this->db->get();
            return $query->result();
        }
        /*funcao para pegar produtos populares*/
        public function produtos_populares()
        {
            $this->db->select(" * ");
            $this->db->from('produtos');
            $this->db->where('produto_tipo', '2');
            $this->db->limit('4');
            $query = $this->db->get();
            return $query->result();
        }
        /*funcao para pegar produtos caracteristcas*/
        public function produtos_destaque()
        {
            $this->db->select(" * ");
            $this->db->from('produtos');
            $this->db->where('produto_tipo', '3');
            $this->db->limit('4');
            $query = $this->db->get();
            return $query->result();
        }
        /*funcoa para o slide*/
        public function get_slider()
        {
            $this->db->select(" * ");
            $this->db->from('slider_images');
//            $this->db->order_by('rand()');
//            $this->db->limit('3');
            $query = $this->db->get();
            return $query->result();
        }
        /*funcao para mostrare os detalhes do produtos*/
        public function detalhe_view_produto($id){
        $this -> db -> select(' * ');
        $this -> db -> from('produtos');
        $this->db->where('idProdutos', $id);
        $query = $this -> db -> get();
        return $query->result();
        }
  /*funcao para receber detalhe dos produtos*/
        public function produtos_detalhes($produto){
        $this -> db -> select(' * ');
        $this -> db -> from('produtos');
        $this->db->where('idProdutos', $produto);
        $query = $this -> db -> get();
        return $query->result();
        }
        /*funcao para gerar novo contato e receber mensagem*/
        public function Inserir_Contato($name,$email,$mensagem){
            $this->db->set('nome', $name);
            $this->db->set('email',$email);
            $this->db->set('mensagem',$mensagem);  
            $this->db->set('data',date("Y-m-d H:i:s"));
            $this->db->insert('contato');
        }
  
}