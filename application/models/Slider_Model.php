<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*CLASSE PARA CRIAÇAÕ DO SLIDE*/
class Slider_Model extends CI_Model {
    
        function __construct() {
        parent::__construct();
		$this->tablename	= $this->session->userdata('table_id').'products';
		$this->tablesubcategory = $this->session->userdata('table_id').'subcategoria';
		$this->tablecategory = $this->session->userdata('table_id').'categoria';
        }
        public function inserir_Slide($nome,$descricao){
            $this->db->set('nome', $nome);
            $this->db->set('descricao', $descricao);
            $this->db->set('criado_data', date("Y-m-d H:i:s"));
            $this->db->set('criado_por', '0');
            $this->db->set('status', 'active');
            $this->db->set('visualizacao', '0');
            $query = $this->db->insert('slider_images');
            $response=$this->db->insert_id();
            return $response;
        }
        function update_image($inserir_id, $mainimagefilename) {
            $this->db->set('imagen', $mainimagefilename);
            $this->db->where('id',$inserir_id);
            $query = $this->db->update('slider_images');
        }
        function get_listar() {
            $this->db->select(" * ");
            $this->db->from('slider_images');
            $query = $this->db->get();
            return $query->result();
        }
      
        function DeleteSlider($id) {
            $this->db->where('id',$id);
            $this->db->delete('slider_images');
        }
      
}