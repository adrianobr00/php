<?php
class Produtos_model extends CI_Model {

    /**
     * author: Adriano ALves
     * email: adrianobr00@gmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
        	$this->tablename = $this->session->userdata('table_id').'produtos';
		$this->tablesubcategoria = $this->session->userdata('table_id').'subcategoria';
		$this->tablecategoria = $this->session->userdata('table_id').'categoria';
        }
    
    
      /*FUNCAO PARA VERIFICAR A EXISTENCIA DE CODIGO NO BANCO, O CODIGO DO PRODUTO DEVE SER UNICO*/    
    function checar_Codigo($code = null) {
        $this->db->where('produto_code', $code);
        $query = $this->db->get('produtos');
        $result = $query->result_array();
        if (!empty($result)) {
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }
    
      /*FUNCAO PARA GERAR LISTA */
        function get_Lista() {
            $this->db->select(" * ");
            $this->db->from($this->tablename);
            $query = $this->db->get();
            return $query->result();
        }

    /*FUNCAO PARA PESQUIZAR PRODUTO PELA DESCRIÇAO CATEGORIA OU PELO CODIGO DE PRODUTO, 
         *  ELE USA O JOIN PARA PUCHAR DA TABELA CATEGORIA OS NOMES RELACIONADOS POR ID*/
        public function get_ProdutoLista($produto_code,$descricao_produto,$categoria_id) {
            $this->db->select($this->tablename.'.*,'.$this->tablecategoria.'.categoria_nome');
            if($produto_code)
            {
                $this->db->like('produto_code',$produto_code);
            }
            if($descricao_produto)
                    $this->db->like('descricao',$descricao_produto);
            if($categoria_id)
                    $this->db->where('idCategoria',$categoria_id);

            $this->db->join($this->tablecategoria,$this->tablecategoria.'.idCategoria='.$this->tablename.'.categoria_id','left');
            $query = $this->db->get($this->tablename);
            return $query->result();
        }
    
    function get($table,$fields,$where='',$perpage=0,$start=0,$one=false,$array='array'){
        
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->order_by('idProdutos','desc');
        $this->db->limit($perpage,$start);
        if($where){
            $this->db->where($where);
        }
        
        $query = $this->db->get();
        
        $result =  !$one  ? $query->result() : $query->row();
        return $result;
    }
    
    /*FUNCAO PARA ATUALIZAR ENDEREÇO DA IMAGEN NO BANCO DE DADOS*/
        function update_Imagem($inserir_id, $mainimagefilename) {
            $this->db->set('product_image', $mainimagefilename);
            $this->db->where('idProdutos',$inserir_id);
            $query = $this->db->update('produtos');
            
        }

    function getById($id){
        $this->db->where('idProdutos',$id);
        $this->db->limit(1);
        return $this->db->get('produtos')->row();
    }
    
    function add($table,$data){
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1')
		{
	    $response=$this->db->insert_id();
            return $response;    
		}
                
		
		return FALSE;       
    }
    
    function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}
		
		return FALSE;       
    }
    
    function delete($table,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
	function count($table){
		return $this->db->count_all($table);
	}
}