<?php
/*
CLASSE PARA EDITAR CRIAR LISTAR E DELETAR SUBCATEGORIAS
 * 
 * CRIADO POR ADRIANO ALVES
 *  */

//
class Subcategoria_Model extends CI_Model
{
	 function __construct() {
        // Call the Model constructor
        parent::__construct();

		$this->table_id = $this->session->userdata('table_id');
		$this->tablename = $this->session->userdata('table_id').'subcategoria';
    }
/*FUNCAO PARA ADICIONAR UMA NOVA CATEGORIA*/
    function adicionar_SubCategoria($id) {     
            $this->db->set('subcategoria_nome', $this->input->post('sub_categoria_nome'));
            $this->db->set('parent_categoria_id',$id);
            $this->db->set('status','0');  
            $this->db->set('criado_por', $this->session->userid);
            $this->db->set('criado_data', date("Y-m-d H:i:s"));
            $query = $this->db->insert('subcategoria');
            $response=$this->db->insert_id();
            return $response;
    }
/*FUNCAO PARA ATUALIZAR IMAGEM DA SUBCATEGORIA*/	
    function update_Image($id, $subfilename) {
        $this->db->set('imagem', $subfilename);
        $this->db->where('subcat_id',$id);
        $query = $this->db->update('subcategoria');
    }
    /*PESQUIZAR SUBCATEGORIA POR ID*/
    function ver_SubCategoria($id) {
        $this->db->where('parent_categoria_id',$id);
        $this->db->where('status','0');
        $query = $this->db->get('subcategoria');
        return $query->result();
    }/*FUNCAO PRA SELECIONAR SUBCATEGORIA PARA EDIÇAO*/
    function edit_SubCategoria($id) {
        $this->db->where('status','0');
        $this->db->where('subcat_id',$id);
        $query = $this->db->get('subcategoria');
        return $query->result();
    }
    /*ENVIAR VALORES ATUALIZADOS PARA O BANCO DE DADOS */
    function edit_SubCategoriaEnviar($inserir_id, $subfilename,$subcategoria_name) {
        $this->db->set('subcategoria_nome', $subcategoria_name);
        $this->db->set('imagem', $subfilename);
        $this->db->set('modifcado_por', $this->session->userid);
        $this->db->set('modificado_data', date("Y-m-d H:i:s"));
        $this->db->where('subcat_id',$inserir_id);
        $query = $this->db->update('subcategoria');
    }
    /*FUNCAO PARA DELETAR SUBCATEGORIA POR ID*/
    function delete_SubCategoria($id) {
       $this->db->set('status', '1');
       $this->db->where('subcat_id',$id);
       $query = $this->db->update('subcategoria');
    }
    /*LISTAR TODAS AS CATEGORIAS ATIVAS*/
    function lista_SubCategoria() {
        $this->db->where('status','0');
        $query = $this->db->get('subcategoria');
        return $query->result();
    }
}