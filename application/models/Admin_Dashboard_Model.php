<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 * 
 * Admin_Dashboard_Model 
 * criado por Adriano Alves
 * Objetivo: gerar relatorios de cliente, de vendas, e de outros
 *  */
class Admin_Dashboard_Model extends CI_Model {
    /*funcao para receber  relatorio de vendas do dia*/
        public function quantidade_vendasDia()
        {
            $date = date("Y-m-d");
            $this->db->select(" * ");
            $this->db->from('tbl_vendas');
            $this->db->where('data_emissao', $date);
            $query = $this->db->get();
            $responce = $query->num_rows();
            return $responce;
        }
        /*funcao para calcular o valor total de vendas
         * **/
        public function soma_Total()
        {
            $this->db->select_sum('valor_total','sum');
            $query = $this->db->get('tbl_vendas');
            return $query->result();
        }
        /*funcao para receber a quantidade de vendas */
        public function contagem_Vendas()
        {
            $this->db->select(" * ");
            $this->db->from('tbl_vendas');
            $query = $this->db->get();
            $responce = $query->num_rows();
            return $responce;
        }
        /*lista de vendas do dia*/
        public function vendas_Dia()
        {
            $date = date("Y-m-d");
            $this->db->select(" * ");
            $this->db->from('tbl_vendas');
            $this->db->where('data_emissao', $date);
            $query = $this->db->get();
            return $query->result();
            
        }
        /*lista de clientes*/
        public function listar_Clientes()
        {
            
            
            $date = date("Y-m-d");
           $query = $this->db->query("SELECT * FROM tbl_clientes WHERE DATE_FORMAT(criacao_data,'%Y-%m-%d') = '$date' ");
               return $query->result();
        }
      
       
}