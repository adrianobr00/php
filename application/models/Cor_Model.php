<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*

 * clase para listar cores editar e excluir
 * criado por Adriano Alves
 *  */
class Cor_Model extends CI_Model {
    /*funcao para listar cor*/
        public function get_lista()
        {
                $this->db->select(" * ");
                $this->db->from('color');
//                $this->db->where('status','0');
                $query = $this->db->get();
                return $query->result();
        }
        
        /*funcao para editar cor**/
        public function editar_Cor($id)
        {
                $this->db->select(" * ");
                $this->db->from('color');
                $this->db->where('id',$id);
                $query = $this->db->get();
                return $query->result();
        }
        /*funcao para adicionar nova cor*/
        public function cor_Add($data)
        {
             $this->db->insert('color', $data); 
             return TRUE;
        }
        /*funcao para enviar e editar no banco a cor*/
        public function editar_CorSubmit($data,$categoria_id)
        {
           $this->db->where('id',$categoria_id);
           $query = $this->db->update('color',$data);
        }
        /*funcao para deletar cor*/
        function deletar_Cor($id) {
            $this->db->where('id',$id);
            $this->db->delete('color');
        }
        
}