<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

        public function __construct()
	{
            parent::__construct();
           
           $this->load->model('Dashboard_Model');
            $this->load->model('Categoria_Model');
            $this->load->model('Subcategoria_Model');
            $this->load->library('cart');
            $this->load->helper('url');  
            $this->load->helper('form');  
            $this->load->library('session');
            $this->load->database();
            $this->load->library('encrypt');
            
        }


        public function index()
        {
             $this->data['categoria_lista'] =  $this->Categoria_Model->get_lista();
             $this->data['slider'] = $this->Dashboard_Model->get_slider();
             $this->data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
             $this->data['produtos_recentes'] = $this->Dashboard_Model->produtos_recentes();
             $this->data['produtos_populares'] = $this->Dashboard_Model->produtos_populares();
             $this->data['produtos_destaque'] = $this->Dashboard_Model->produtos_destaque();
             $this->data['view'] = 'home';
             $this->load->view('tema/header',$this->data);
             $this->load->view('tema/rodape',$this->data);
             
           
        }
	
        public function mobile_shop()
        {
            $data['categoria_lista'] = $this->Categoria_Model->get_lista();
            $data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
            $this->load->view('mobile_shop',$data);
        }
	
	public function cart()
	{
            $data['categoria_lista'] = $this->Categoria_Model->get_list();
            $data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
            $this->load->view('cart',$data);
        }
        
        public function product_detail($product)
	{
            $data['products'] = $this->dashboard_model->get_all();
            $data['products_detail'] = $this->dashboard_model->products_detail($product);
            $this->load->view('product_detail',$data);  
        }
        public function shipping()
	{
            $data['categoria_lista'] = $this->Categoria_Model->get_list();
            $data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
            $this->load->view('shipping',$data);  
        }
        public function term()
	{
            $data['categoria_lista'] = $this->Categoria_Model->get_list();
            $data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
            $this->load->view('term',$data);  
        }
        public function faqs()
	{
            $data['categoria_lista'] = $this->Categoria_Model->get_list();
            $data['subcate'] = $this->Subcategoria_Model->lista_SubCategoria();
            $this->load->view('faqs',$data);  
        }
        
}
