<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*CONTROLLER QUE IRAR GENRENCIAR TODA A CLASSE CATEGORIA E SUBCATEGORIA 

 * 
 * CRIADO POR ADRIANO ALVES */
class Categoria extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Categoria_Model');
        $this->load->model('Common_Modal');
        $this->load->model('Subcategoria_Model');
        $this->load->helper(array('form', 'codegen_helper'));
        if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }
    }
    public function index()
    {
       $this->data['categoria_lista'] = $this->Categoria_Model->get_lista();
        $this->data['view'] = 'admin/categoria/lista';
       	$this->load->view('admin/tema/header',$this->data);
        
        
    }   
/*FUNCAO PARA ADICIONAR UMA NOVA CATEGORIA */
   public function categoria_Add()
   {
        $this->form_validation->set_rules('categoria_nome', 'Categoria Nome', 'required');
        if ($this->form_validation->run() == FALSE)
        {
           
                $this->data['view'] = 'admin/categoria/add';
         	$this->load->view('admin/tema/header',$this->data);
        
        }
        else
        {
            $categoria_nome = strtolower($this->input->post('categoria_nome'));
            $user_id = $this->session->userid;
            $data = array(
                'categoria_nome' => $categoria_nome,
                'criado_por' => $user_id,
                'criacao_data' => date("Y-m-d H:i:s"),
                'status' => '0'
            );
            $insert = $this->Categoria_Model->Categoria_Add($data);
            $this->session->set_flashdata('sucess','Categoria Criado sucesso!!! :D');
           redirect('account/categoria');

        }

   }
/*FUNCAO PARA ADICIONAR UMA NOVA SUBCATEGORIA */      
    public function add_SubCategoria($id) {
        if(!empty($_POST))
        {		
            $this->form_validation->set_rules('sub_categoria_nome', 'Sub Categoria Nome', 'trim|required');
            
                   
            if ($this->form_validation->run() == FALSE)
            {
                $this->data['view'] = 'admin/subcategoria/add';
         	$this->load->view('admin/tema/header',$this->data);
        
            }
            else
            {    
                
             
                $insert_id = $this->Subcategoria_Model->adicionar_SubCategoria($id);
//                    @mkdir('./upload/subcategory/'.$insert_id.'/',0777,true);
                if(!$_FILES['subimage']['name']==null){
                @mkdir('./upload/subcategoria/'.$insert_id);
//                    @chmod('./upload/subcategory/'.$insert_id.'/',0777);
                $config['upload_path'] = './upload/subcategoria/'.$insert_id.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $config['file_name'] = strtolower($_FILES['subimage']['name']);
                $this->upload->initialize($config);
                $this->upload->do_upload('subimage');
                $upload_data	= $this->upload->data();
                $subfilename	= $upload_data['file_name'];
                $this->Common_Modal->compress_image('./upload/subcategoria/'.$insert_id.'/'.$subfilename, './upload/subcategoria/'.$insert_id.'/'.$subfilename, 50);
//                    $this->compress_image('./upload/subcategory/'.$insert_id.'/'.$subfilename, './upload/subcategory/'.$insert_id.'/'.$subfilename, 50); 
                $this->Subcategoria_Model->update_image($insert_id, $subfilename);}
                $this->session->set_flashdata('sucess','Subcategoriaa adicionada com Sucesso');
                redirect('account/categoria');
            }
        }
        else
        {
                $this->data['view'] = 'admin/subcategoria/add';
         	$this->load->view('admin/tema/header',$this->data);
        }
    }
/*FUNCAO PARA ADICIONAR VER UMA SUBCATEGORIA */      
    public function ver_SubCategoria($id) {
          $this->data['view'] = 'admin/subcategoria/lista';
          $this->data['subCategoriaLista'] = $this->Subcategoria_Model->ver_SubCategoria($id);
         $this->load->view('admin/tema/header',$this->data);
           
          
    }
/*FUNCAO PARA EDITAR UMA NOVA CATEGORIA */
    public function editar_SubCategoria($id) {
        if(!empty($_POST))
        {
            $this->form_validation->set_rules('sub_categoria_nome', 'Sub Categoria Nome', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                $this->data['view'] = 'admin/subcategoria/editar';
                $this->data['editarSubCategoria'] = $this->Subcategoria_Model->edit_SubCategoria($id);
                $this->load->view('admin/tema/header',$this->data);
            }
            else
            {
                $insert_id = $this->input->post('insertid');   
                $subcategoria_nome = $this->input->post('sub_categoria_nome');   
                if($_FILES['subimage']['name']=='')
                {
                    $subfilename = $this->input->post('sub_categoria_imagem');   
                }
                else
                {
                    $config['upload_path'] = './upload/subcategoria/'.$insert_id.'/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $this->load->library('upload', $config);
                    $config['file_name'] = strtolower($_FILES['subimage']['name']);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('subimage');
                    $upload_data	= $this->upload->data();
                    $subfilename	= $upload_data['file_name'];
                    $this->Common_Modal->compress_image('./upload/subcategoria/'.$insert_id.'/'.$subfilename, './upload/subcategoria/'.$insert_id.'/'.$subfilename, 50);
                }

                $this->Subcategoria_Model->edit_SubCategoriaEnviar($insert_id, $subfilename,$subcategoria_nome);
                $this->session->set_flashdata('sucess','Subcategoria Atualizada com Sucesso!! :D');
                redirect('account/categoria');
            }
        } 
        else 
        {
               $this->data['view'] = 'admin/subcategoria/editar';
                $this->data['editarSubCategoria'] = $this->Subcategoria_Model->edit_SubCategoria($id);
                $this->load->view('admin/tema/header',$this->data);
           
        }
    }
    /*FUNCAO PARA EDITAR CATEGORIA */
    public function editar_Categoria($id) {
        if(!empty($_POST))
        {
            $this->form_validation->set_rules('categoria_nome', 'Categoria Nome', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                
                 $this->data['view'] = 'admin/categoria/editar';
                $this->data['editarCategoria'] = $this->Categoria_Model->editar_Categoria($id);
                $this->load->view('admin/tema/header',$this->data);
                                
            }
            else
            {
                $categoria_nome = strtolower($this->input->post('categoria_nome'));
                $user_id = $this->session->userid;
                $categoria_id = $this->input->post('categoria_id');
                $data = array(
                    'categoria_nome' => $categoria_nome,
                    'modificado_por' => $user_id,
                    'modificado_data' => date("Y-m-d H:i:s"),
                    'status' => '0'
            );
                $this->Categoria_Model->editar_CategoriaEnviar($data,$categoria_id);
                $this->session->set_flashdata('sucess','Categoria Atualizada com sucesso!!:D');
                redirect('account/categoria');
            }
        } 
        else 
        {
                $this->data['view'] = 'admin/categoria/editar';
                $this->data['editarCategoria'] = $this->Categoria_Model->editar_Categoria($id);
                $this->load->view('admin/tema/header',$this->data);
        }
    }
    /*FUNCAO PARA DELETAR UMA SUB CATEGORIA */

    public function deletar_SubCategoria($id) 
    {
        $this->Subcategoria_Model->delete_SubCategoria($id);
        $this->session->set_flashdata('sucess','Subcategoria deletada com sucesso!! :D');
        redirect('account/categoria');
    }
          /*FUNCAO PARA DELETAR UMA  CATEGORIA */
    public function deletar_Categoria($id) 
    {
        $this->Categoria_Model->deletar_Categoria($id);
        $this->session->set_flashdata('sucess','Categoria deletada com sucesso!! :D');
        redirect('account/categoria');
    }
          /*FUNCAO PARA ALTERAR OS STATUS DE UMA CATEGORIA */
    public function categoria_Status($id,$status) 
    {
        $this->Categoria_Model->categoria_Status($id,$status);
        $this->session->set_flashdata('sucess','Categoria Atualizada com sucesso!!:D');
        redirect('account/categoria');
    }
}
