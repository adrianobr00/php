<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*CONTROLLER QUE IRAR GENRENCIAR TODA A CLASSE e GERENCIAMENTO DA COR
 * 
 * CRIADO POR ADRIANO ALVES */
class Marca extends CI_Controller {

        public function __construct()
	{
            parent::__construct();
            
        
            $this->load->model('Common_Modal');
            $this->load->model('Marca_Model');
           if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }
    }
        
        public function index()
        {
              $this->data['marca_lista'] = $this->Marca_Model->get_lista();
              $this->data['view'] = 'admin/marca/lista';
              $this->load->view('admin/tema/header',$this->data);
              
        }   
       public function Add(){
       
       $this->form_validation->set_rules('marca', ' Nome da marca', 'required');
       
        if ($this->form_validation->run() == FALSE)
        {
           
                $this->data['view'] = 'admin/marca/add';
         	$this->load->view('admin/tema/header',$this->data);
        
        }
        else
        {
         
            $this->load->library('form_validation');
               $data = array(
                'marca' => set_value('marca'),
                'logo' => $_FILES['product_image']['name'],
                'cadastro'=> date("Y-m-d H:i:s"),   
                'status'=> '0'
                 
        );   
                $inserir_id = $this->Marca_Model->add_Marca($data);
                if ($inserir_id  == TRUE) {
                @mkdir('./upload/marcas/');
                $config['upload_path'] = './upload/marcas/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $config['file_name'] = ($_FILES['product_image']['name']);
                $this->upload->initialize($config);
                $this->upload->do_upload('product_image');
                $upload_data	= $this->upload->data();
                $mainimagefilename	= $upload_data['file_name'];
                $this->Common_Modal->compress_image('./upload/marcas/'.$mainimagefilename, './upload/marcas/'.$mainimagefilename, 50);
                $this->Marca_Model->update_Image($inserir_id, $mainimagefilename);
                $this->session->set_flashdata('sucess','Marca adicionado com sucesso');
                 redirect(base_url('admin/marca/'));}
                
             else {
                  $this->session->set_flashdata('erro','Erro ao Cardastrar imagem');
                 redirect(base_url('admin/marca/add/'));
            }        
        
        $this->session->set_flashdata('erro','Erro ao Cardastrar imagem');
                 redirect(base_url('admin/marca/add/'));
        
     
       }}
         
       
             

       
        public function editar_marca($id) {
            if(!empty($_POST))
            {
            $this->form_validation->set_rules('cor_nome', 'Nome da Cor', 'required');
            $this->form_validation->set_rules('cor_code', 'Codigo da Cor', 'required');
            if ($this->form_validation->run() == FALSE)
                {
                $this->data['editar_Cor'] = $this->Cor_Model->editar_Cor($id);
                $this->data['view'] = 'admin/cor/edita';
                $this->load->view('admin/tema/header',$this->data);
                    
                }
                else
                {
                    $categoria_id = $this->input->post('cor_id');
                    $cor_nome = strtolower($this->input->post('cor_nome'));
                    $user_id = $this->session->userid;
                    $data = array(
                            'cor_nome' => $cor_nome,
                            'cor_code' => $this->input->post('cor_code'),
                            'modificado_por' => $user_id,
                            'modificado_data' => date("Y-m-d H:i:s"),
                            'status' => '1'
                    );
                    
                    $this->Cor_Model->editar_CorSubmit($data,$categoria_id);
                    $this->session->set_flashdata('sucess','Cor ATUALIZADA COM SUCESSO!! :D');
                    redirect('account/cor');
                }
               
            } 
            else 
            {
                $this->data['editar_Cor'] = $this->Cor_Model->editar_Cor($id);
                $this->data['view'] = 'admin/cor/edita';
                $this->load->view('admin/tema/header',$this->data);
            }
        }
 
      
        public function deletar() 
        {
         $id= $this->input->post('idProtuc');
         $foto=$this->input->post('id');
         $caminho =  'upload/marcas/'.$foto;
       
        
                  
            
        if( file_exists( $caminho) )
           { 
           if((unlink( $caminho )&& $this->Marca_Model->deletar_marca($id))){
            $this->session->set_flashdata('sucess','Marca excluido com sucesso.');
            redirect('admin/marca');
           }
          else{
           $this->session->set_flashdata('error','Erro ao escluir a marca.'); 
          redirect('admin/marca');
        }
                 
        }}
        

         
         
         
     }



