<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

        public function __construct()
	{
            parent::__construct();
            
            
            $this->load->model('Categoria_Model','categoria_model');
            $this->load->model('Subcategoria_Model','Subcategoria_Model');
            $this->load->model('Slider_Model','Slider_Model');
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->database();
          
            $this->load->model('Common_Modal');
             if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }
	}
        
        public function index()
        {
            $this->data['slider_lista'] = $this->Slider_Model->get_listar();
            $this->data['view'] =('admin/slider/lista');
            $this->load->view('admin/tema/header',$this->data);
            
            
        }   
        public function DeleteSlider($id)
        {
            
            
            $this->Slider_Model->DeleteSlider($id);
            $this->session->set_flashdata('sucess','Slider Deletado com sucesso!!:D');
            redirect('account/lista-slider');
        }   

       public function add_Slider()
       {
           if(!empty($_POST))
           {
                $insert_id = $this->Slider_Model->inserir_Slide();
                @mkdir('./upload/slider/'.$insert_id);
                $config['upload_path'] = './upload/slider/'.$insert_id.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $config['file_name'] = strtolower($_FILES['slider_image']['name']);
                $this->upload->initialize($config);
                $this->upload->do_upload('slider_image');
                $upload_data	= $this->upload->data();
                $mainimagefilename	= $upload_data['file_name'];
                $this->Common_Modal->compress_image('./upload/slider/'.$insert_id.'/'.$mainimagefilename, './upload/slider/'.$insert_id.'/'.$mainimagefilename, 50);
                $this->Slider_Model->update_image($insert_id, $mainimagefilename);
                $this->session->set_flashdata('SUCCESSMSG','Slider Imagen adicionada com sucesso');
                redirect('account/lista-slider');
           }
           else
           {
                
            $this->data['view'] =('admin/slider/add');
            $this->load->view('admin/tema/header',$this->data);
           }
       }
}
