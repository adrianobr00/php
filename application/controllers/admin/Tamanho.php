<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tamanho extends CI_Controller {

        public function __construct()
	{
            parent::__construct();
            
            $this->load->model('Categoria_Model');
            $this->load->model('Common_Modal');
            $this->load->model('Subcategoria_Model');
            $this->load->model('Tamanho_Model');
            $this->load->helper(array('form', 'codegen_helper'));
        if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }
    }
        
        public function index()
        {
             $this->data['tamanho_lista'] = $this->Tamanho_Model->get_lista();
             $this->data['view'] = 'admin/tamanho/lista';
       	     $this->load->view('admin/tema/header',$this->data);
          
        }
       public function add_tamanho()
       {
            $this->form_validation->set_rules('tamanho_nome', 'Tamanho', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                
           
             $this->data['view'] = 'admin/tamanho/add';
       	     $this->load->view('admin/tema/header',$this->data);
              
            }
            else
            {
                $tamanho_nome= strtolower($this->input->post('tamanho_nome'));
                $user_id = $this->session->userid;
                $data = array(
                        'tamanho' => $tamanho_nome,
                        'criado_por' => $user_id,
                        'criado_data' => date("Y-m-d H:i:s"),
                        'status' => '1'
                );
                $insert = $this->Tamanho_Model->add_Tamanho($data);
                $this->session->set_flashdata('sucess','Tamanho criado com sucesso!!:D');
                redirect('account/tamanho');
            }
            
       }
        public function editar_Tamanho($id) {
            if(!empty($_POST))
            {
                 $this->form_validation->set_rules('tamanho_nome', 'Tamanho', 'required');
                
                if ($this->form_validation->run() == FALSE)
                {
                     $this->data['editartamanho'] = $this->Tamanho_Model->editar_TamanhoSelecionado($id);
                    $this->data['view']=('admin/tamanho/edita'); 
              	     $this->load->view('admin/tema/header',$this->data);}
                else
                {
                    $categoria_id = $this->input->post('tamanho_id');
                    $tamanho_nome = strtolower($this->input->post('tamanho_nome'));
                    $user_id = $this->session->userid;
                    $data = array(
                            'tamanho' => $tamanho_nome,
                            'modificado_por' => $user_id,
                            'modificado_data' => date("Y-m-d H:i:s"),
                            'status' => '1'
                    );
                    
                    $this->Tamanho_Model->edita_TamanhoEnviar($data,$categoria_id);
                    $this->session->set_flashdata('sucess','Tamanho atualizado com sucesso !! :D');
                    redirect('account/tamanho');
                }
               
            } 
            else 
            {
                 $this->data['editartamanho'] = $this->Tamanho_Model->editar_TamanhoSelecionado($id);
                    $this->data['view'] = ('admin/tamanho/edita'); 
              	     $this->load->view('admin/tema/header',$this->data);
            }
        }
 
      
        public function deletar_Tamanho($id) 
        {
            $this->Tamanho_Model->deletar_Tamanho($id);
            $this->session->set_flashdata('sucess','Tamanho atualizado com sucesso!!');
            redirect('account/tamanho');
        }
       
}
