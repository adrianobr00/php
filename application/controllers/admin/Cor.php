<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*CONTROLLER QUE IRAR GENRENCIAR TODA A CLASSE e GERENCIAMENTO DA COR
 * 
 * CRIADO POR ADRIANO ALVES */
class Cor extends CI_Controller {

        public function __construct()
	{
            parent::__construct();
            
            $this->load->model('Categoria_Model');
            $this->load->model('Common_Modal');
            $this->load->model('Subcategoria_Model');
            $this->load->model('Cor_Model');
           if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }
    }
        
        public function index()
        {
              $this->data['cor_lista'] = $this->Cor_Model->get_lista();
              $this->data['view'] = 'admin/cor/lista';
              $this->load->view('admin/tema/header',$this->data);
              
        }   
       public function cor_Add()
       {
            $this->form_validation->set_rules('cor_nome', 'Nome da Cor', 'required');
            $this->form_validation->set_rules('cor_code', 'Codigo da Cor', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $this->data['view'] = 'admin/cor/add';
              $this->load->view('admin/tema/header',$this->data);
            }
            else
            {
                $cor_nome = strtolower($this->input->post('cor_nome'));
                $user_id = $this->session->userid;
                $data = array(
                        'cor_nome' => $cor_nome,
                        'cor_code' => $this->input->post('cor_code'),
                        'criado_por' => $user_id,
                        'criado_data' => date("Y-m-d H:i:s"),
                        'status' => '1'
                );
                $insert = $this->Cor_Model->cor_add($data);
                $this->session->set_flashdata('sucess','Cor Adicionada com sucesso !!! :D');
                redirect('account/cor-add');
            }
            
       }
        public function editar_Cor($id) {
            if(!empty($_POST))
            {
            $this->form_validation->set_rules('cor_nome', 'Nome da Cor', 'required');
            $this->form_validation->set_rules('cor_code', 'Codigo da Cor', 'required');
            if ($this->form_validation->run() == FALSE)
                {
                $this->data['editar_Cor'] = $this->Cor_Model->editar_Cor($id);
                $this->data['view'] = 'admin/cor/edita';
                $this->load->view('admin/tema/header',$this->data);
                    
                }
                else
                {
                    $categoria_id = $this->input->post('cor_id');
                    $cor_nome = strtolower($this->input->post('cor_nome'));
                    $user_id = $this->session->userid;
                    $data = array(
                            'cor_nome' => $cor_nome,
                            'cor_code' => $this->input->post('cor_code'),
                            'modificado_por' => $user_id,
                            'modificado_data' => date("Y-m-d H:i:s"),
                            'status' => '1'
                    );
                    
                    $this->Cor_Model->editar_CorSubmit($data,$categoria_id);
                    $this->session->set_flashdata('sucess','Cor ATUALIZADA COM SUCESSO!! :D');
                    redirect('account/cor');
                }
               
            } 
            else 
            {
                $this->data['editar_Cor'] = $this->Cor_Model->editar_Cor($id);
                $this->data['view'] = 'admin/cor/edita';
                $this->load->view('admin/tema/header',$this->data);
            }
        }
 
      
        public function deletar_Cor($id) 
        {
            $this->Cor_Model->deletar_Cor($id);
            $this->session->set_flashdata('sucess','Cor Deletada com Successo!! :D');
            redirect('account/cor');
        }
       
}
