<?php

class Produtos extends CI_Controller {
    
    /**
     * author: Ramon Silva 
     * email: adrianobr00@gmail.com
     * 
     */
    
    function __construct() {
        parent::__construct();
        if( (!session_id()) || (!$this->session->userdata('logado'))){
             redirect('admin/dashboard/login');
        }

        $this->load->helper(array('form', 'codegen_helper'));
        $this->load->model('produtos_model', '', TRUE); 
        $this->load->model('Categoria_Model','categoria_model');
         $this->load->model('Subcategoria_Model','Subcategoria_Model');
          $this->load->model('Common_Modal');
           $this->load->model('Galeria_model');
        $this->data['menuProdutos'] = 'Produtos';
    }

    function index(){
	   $this->gerenciar();
            $produto_code           = trim($this->input->get('produto_code'));
            $descricao_produto    = trim($this->input->get('descricao_produto'));
            $categoria_id            = trim($this->input->get('produto_categoria'));
            $output['produto_lista'] = $this->produtos_model->get_ProdutoLista($produto_code,$descricao_produto,$categoria_id) ;
            $output['tamanho_lista'] = $this->categoria_model->get_ListaTamanho();
            $output['cor_lista'] = $this->categoria_model->get_lista_cor();
            $output['categoria_lista'] = $this->categoria_model->get_lista();
         
    }
    
    function galeria(){
        $idProdutos = $this->uri->segment(4);
        $this->data['results'] = $this->Galeria_model->get('fotos','idFotos,foto_url,foto_data,idProdutos',$idProdutos,'','');
        $this->data['view'] = 'admin/produtos/galeria';
        $this->load->view('admin/tema/header', $this->data);
     
    }
    
      function add_galeria(){
             $idProdutos = $this->input->post('idProdutos');
            
            $this->load->library('form_validation');
               $data = array(
                'idProdutos' => set_value('idProdutos'),
                'foto_url' => $_FILES['product_image']['name'],
                'foto_data'=> date("Y-m-d H:i:s")       
                 
        );   
                $inserir_id = $this->Galeria_model->add('fotos', $data);
                if ($inserir_id  == TRUE) {
                @mkdir('./upload/produto/'.$idProdutos);
                $config['upload_path'] = './upload/produto/'.$idProdutos.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $config['file_name'] = ($_FILES['product_image']['name']);
                $this->upload->initialize($config);
                $this->upload->do_upload('product_image');
                $upload_data	= $this->upload->data();
                $mainimagefilename	= $upload_data['file_name'];
                $this->Common_Modal->compress_image('./upload/produto/'.$idProdutos.'/'.$mainimagefilename, './upload/produto/'.$idProdutos.'/'.$mainimagefilename, 50);
                $this->produtos_model->update_Imagem($inserir_id, $mainimagefilename);
                $this->session->set_flashdata('sucess','Produto adicionado com sucesso');
                 redirect(base_url('admin/produtos/galeria/'.$idProdutos));}
                
             else {
                  $this->session->set_flashdata('erro','Erro ao Cardastrar imagem');
                 redirect(base_url('admin/produtos/galeria/'.$idProdutos));
            }
          
        
        
        
     
    }
     function excluirImagen(){
         $idProduto=$this->input->post('idProtuc');
         $idFotos=$this->input->post('id');
         $caminho = $this->input->post('caminho');
       
      
      
         if($this->Galeria_model->deleteFoto($caminho, $idFotos)){
         if ( file_exists( $caminho ) )
           {
            if(unlink( $caminho )){
                  $this->session->set_flashdata('sucess','Imagen Excluida Com sucesso.');
                  redirect(base_url('admin/produtos/galeria/'.$idProduto));
            }
          }else{
          $this->session->set_flashdata('error','Erro ao escluir a imagems.'); 
          redirect(base_url('admin/produtos/galeria/'.$idProduto));}
         
         }
         else{
             $this->session->set_flashdata('error','Erro ao escluir a imagems.'); 
             redirect(base_url('admin/produtos/galeria/'.$idProduto));
         }
         
         
         
     }
    
                function gerenciar(){
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url());
        }

        $this->load->library('table');
        $this->load->library('pagination');
        
        
        $config['base_url'] = base_url().'admin/produtos/gerenciar/';
        $config['total_rows'] = $this->produtos_model->count('produtos');
        $config['per_page'] = 10;
        $config['next_link'] = 'Próxima';
        $config['prev_link'] = 'Anterior';
        $config['full_tag_open'] = '<div class="pagination alternate"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a style="color: #2D335B"><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = 'Primeira';
        $config['last_link'] = 'Última';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        
        $this->pagination->initialize($config); 	

	    $this->data['results'] = $this->produtos_model->get('produtos','idProdutos,descricao,unidade,precoCompra,precoVenda,estoque,estoqueMinimo','',$config['per_page'],$this->uri->segment(3));
       
	    $this->data['view'] = 'admin/produtos/produtos';
       	$this->load->view('admin/tema/header',$this->data);
       
		
    }
	
    function adicionar() {

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'aProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para adicionar produtos.');
           redirect(base_url());
        }

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $this->data['tamanho_lista'] = $this->categoria_model->get_ListaTamanho();
        $this->data['cor_lista'] = $this->categoria_model->get_lista_cor();
        $this->data['produto_lista'] = $this->produtos_model->get_lista();
        $this->data['categoria_lista'] = $this->categoria_model->get_lista();
        
        if ($this->form_validation->run('produtos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);
            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);
             if(!empty($this->input->post('sub_categoria_id')))
                {
                    $sub_categoria_id = $this->input->post('sub_categoria_id');
                }
             else
                {
                    $sub_categoria_id= '';
                }    
           
            
            $data = array(
                'produto_code' => set_value('produto_code'),
                'produto_nome' => set_value('produto_nome'),
                'categoria_id' => set_value('produto_categoria'),
                'sub_categoria_id'=> $sub_categoria_id,
                'produtos_relacionados'=> set_value('produto_tipo'),
                'color_id'=> set_value('cor_produto'),
                'tamanho_id'=> set_value('tamanho'),
                'descricao' => set_value('descricao'),
                'unidade' => set_value('unidade'),
                'precoCompra' => $precoCompra,
                'precoVenda' => $precoVenda,
                'estoque' => set_value('estoque'),
                'estoqueMinimo' => set_value('estoqueMinimo'),
                'saida' => set_value('saida'),
                'entrada' => set_value('entrada'),
                'product_image'=> set_value('product_image'),
                'criado_data' => date("Y-m-d H:i:s"),
                'status' => '0',
                'criado_por' =>  $this->session->userdata('id')
                
                
            );
         
                $inserir_id = $this->produtos_model->add('produtos', $data);
                if ($inserir_id  == TRUE) {
                @mkdir('./upload/produto/'.$inserir_id);
                $config['upload_path'] = './upload/produto/'.$inserir_id.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $config['file_name'] = ($_FILES['product_image']['name']);
                $this->upload->initialize($config);
                $this->upload->do_upload('product_image');
                $upload_data	= $this->upload->data();
                $mainimagefilename	= $upload_data['file_name'];
                $this->Common_Modal->compress_image('./upload/produto/'.$inserir_id.'/'.$mainimagefilename, './upload/produto/'.$inserir_id.'/'.$mainimagefilename, 50);
                $this->produtos_model->update_Imagem($inserir_id, $mainimagefilename);
                $this->session->set_flashdata('sucess','Produto adicionado com sucesso');
                redirect('admin/produtos');
                $this->session->set_flashdata('success','Produto adicionado com sucesso!');
                redirect(base_url() . 'admin/produtos/adicionar/');
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';
            }
        }
        $this->data['view'] = 'admin/produtos/adicionarProduto';
        $this->load->view('admin/tema/header', $this->data);
     
    }
    
     /*LISTA SUBCATEGORIA*/
        public function ver_SubCategoria()
        {
            $id = $this->input->post('categoria_id');
            $response = $this->Subcategoria_Model->ver_SubCategoria($id);
            $htmlvalue='';
             if(!empty($response))
             {
                     foreach($response as $valCat) { 
                        $htmlvalue.='<option style="text-transform: capitalize;" value="'.$valCat->subcat_id.'">'.$valCat->subcategoria_nome.'</option>'; 
                     }
                     $html = '<label for="descricao" class="control-label">Sub Categoria: <span class="required">*</span></label>   <select name="sub_categoria_id" class="form-control" multiple><option value="">Selecione uma SubCategoria</option>'.$htmlvalue.'</select> ';  
             }
             else 
             {
                 $html = '';  
             }
             $data['html'] = $html;
             echo json_encode($data);
             
        }
    
     /*GERADOR DE CODIGO*/   
        function gerador_code() {
            $pass = "";
            $chars = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

            for ($i = 0; $i < 15; $i++) {
                $pass .= $chars[mt_rand(0, count($chars) - 1)];
            }
            return $pass;
        }
        
        //**VERIFICA SE O CODIGO GERADO JA ESTAR CADASTRADO NO BACNO*/
        public function produto_codigo()
        {
            $code = $this->gerador_code();
            
            $response = $this->produtos_model->checar_Codigo($code);
            ($response);
            if ($response) {
                    $this->produto_codigo();
               } else {
                   echo $code;
                   exit;
               }
               exit;
        }

    function editar() {

        if(!$this->uri->segment(4) || !is_numeric($this->uri->segment(4))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('admin');
        }

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'eProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para editar produtos.');
           redirect(base_url(admin));
        }
        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $this->data['tamanho_lista'] = $this->categoria_model->get_ListaTamanho();
        $this->data['cor_lista'] = $this->categoria_model->get_lista_cor();
        $this->data['produto_lista'] = $this->produtos_model->get_lista();
        $this->data['categoria_lista'] = $this->categoria_model->get_lista();
        
        if ($this->form_validation->run('produtos') == false) {
            $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">' . validation_errors() . '</div>' : false);
        } else {
            $precoCompra = $this->input->post('precoCompra');
            $precoCompra = str_replace(",","", $precoCompra);
            $precoVenda = $this->input->post('precoVenda');
            $precoVenda = str_replace(",", "", $precoVenda);
             if(!empty($this->input->post('sub_categoria_id')))
                {
                    $sub_categoria_id = $this->input->post('sub_categoria_id');
                }
             else
                {
                    $sub_categoria_id= '';
                }    
           
            
            $data = array(
                'produto_code' => set_value('produto_code'),
                'produto_nome' => set_value('produto_nome'),
                'categoria_id' => set_value('produto_categoria'),
                'sub_categoria_id'=> $sub_categoria_id,
                'produtos_relacionados'=> set_value('produtos_relacionados'),
                'produto_tipo'=> set_value('produto_tipo'),
                'color_id'=> set_value('cor_produto'),
                'tamanho_id'=> set_value('tamanho'),
                'descricao' => set_value('descricao'),
                'unidade' => set_value('unidade'),
                'precoCompra' => $precoCompra,
                'precoVenda' => $precoVenda,
                'estoque' => set_value('estoque'),
                'estoqueMinimo' => set_value('estoqueMinimo'),
                'saida' => set_value('saida'),
                'entrada' => set_value('entrada'),
                'criado_data' => date("Y-m-d H:i:s"),
                'status' => '0',
                'criado_por' =>  $this->session->userdata('id')
                
                
            );
                    $insert_id = $this->input->post('idProdutos');
                    if($_FILES['product_image']['name']=='')
                    {
                        $mainimagefilename = strtolower($_FILES['product_image']['name']);
                    }
                    else
                    {
                          
                        $config['upload_path'] = './upload/produto/'.$insert_id.'/';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $this->load->library('upload', $config);
                        $config['file_name'] = strtolower($_FILES['product_image']['name']);
                        $this->load->helper('file');
                        delete_files(FCPATH .  $config['upload_path']);
                        $this->upload->initialize($config);
                        $this->upload->do_upload('product_image');
                        $upload_data	= $this->upload->data();
                        $mainimagefilename	= $upload_data['file_name'];
                        $this->produtos_model->update_Imagem($insert_id, $mainimagefilename);
                        $this->Common_Modal->compress_image('./upload/produto/'.$insert_id.'/'.$mainimagefilename, './upload/produto/'.$insert_id.'/'.$mainimagefilename, 50);
                    }
                        if ($this->produtos_model->edit('produtos', $data, 'idProdutos', $this->input->post('idProdutos')) == TRUE) {
                $this->session->set_flashdata('success','Produto editado com sucesso!');
                redirect(base_url() . 'admin/produtos/editar/'.$this->input->post('idProdutos'));
            } else {
                $this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';
            }
        }

        $this->data['result'] = $this->produtos_model->getById($this->uri->segment(4));

        $this->data['view'] = 'admin/produtos/editarProduto';
        $this->load->view('admin/tema/header', $this->data);
     
    }


    function visualizar() {
        
        if(!$this->uri->segment(4) || !is_numeric($this->uri->segment(4))){
            $this->session->set_flashdata('error','Item não pode ser encontrado, parâmetro não foi passado corretamente.');
            redirect('admin');
        }
        
        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'vProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para visualizar produtos.');
           redirect(base_url('admin'));
        }

        $this->data['result'] = $this->produtos_model->getById($this->uri->segment(4));

        if($this->data['result'] == null){
            $this->session->set_flashdata('error','Produto não encontrado.');
            redirect(base_url() . 'admin/produtos/editar/'.$this->input->post('idProdutos'));
        }

        $this->data['view'] = 'admin/produtos/visualizarProduto';
        $this->load->view('admin/tema/header', $this->data);
     
    }
	
    function excluir(){

        if(!$this->permission->checkPermission($this->session->userdata('permissao'),'dProduto')){
           $this->session->set_flashdata('error','Você não tem permissão para excluir produtos.');
           redirect(base_url());
        }

        
        $id =  $this->input->post('id');
        if ($id == null){

            $this->session->set_flashdata('error','Erro ao tentar excluir produto.');            
            redirect(base_url().'admin/produtos/gerenciar/');
        }

        $this->db->where('produtos_id', $id);
        $this->db->delete('produtos_os');


        $this->db->where('produtos_id', $id);
        $this->db->delete('itens_de_vendas');
        
        $this->produtos_model->delete('produtos','idProdutos',$id);             
        

        $this->session->set_flashdata('success','Produto excluido com sucesso!');            
        redirect(base_url().'admin/produtos/gerenciar/');
    }
}

